#!/bin/bash
set -e

#REL_DD4hep=$(ls /tmp/rpmbuild/RPMS/noarch/*DD4hep* /tmp/rpmbuild/RPMS/noarch/*yamlcpp*)
REL_DD4hep=$(ls /tmp/rpmbuild/RPMS/noarch/*DD4hep*)
#REL_GitCondDB=$(ls /tmp/rpmbuild/RPMS/noarch/*GitCondDB*)
#REL_pydantic=$(ls /tmp/rpmbuild/RPMS/noarch/*pydantic*)
#REL_ruamel=$(ls /tmp/rpmbuild/RPMS/noarch/*ruamel*)
echo "Going to release:"
for f in ${REL_DD4hep} ${REL_GitCondDB} ${REL_pydantic} ${REL_ruamel}; do 
echo "- ${f}"
done

TARGET=/eos/project/l/lhcbwebsites/www/lhcb-rpm/incubator
for f in ${REL_DD4hep} ${REL_GitCondDB} ${REL_pydantic} ${REL_ruamel}; do 
echo "copying ${f} to ${TARGET}"
cp  ${f} ${TARGET}
done

echo "Updating RPM metadata"
createrepo --update ${TARGET}

