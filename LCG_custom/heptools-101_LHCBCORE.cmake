#--- Version ------------------------------------------------------
set(heptools_version 101_LHCBCORE)

#---Core layer-----------------------------------------------------
include(heptools-101)

#---DD4hep Package---------------------------------------------
# Disable link with Geant4 and recompile...
set(DD4hep_01.19_geant4 "OFF")
LCG_external_package(DD4hep 01.19)
# Specific recipe to build DD4hep with GEANT4 UNITS
# Comment out otherwise
LCG_user_recipe(
    DD4hep
    URL ${GenURL}/DD4hep-${DD4hep_native_version}.tar.gz
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DBOOST_ROOT=<Boost_home>
               -DCMAKE_CXX_STANDARD=<CMAKE_CXX_STANDARD>
               -DDD4HEP_USE_XERCESC=ON
               -DXERCESC_ROOT_DIR=<XercesC_home>
               -DROOTSYS=${ROOT_home}
               IF hepmc3_native_version THEN
                  -DDD4HEP_USE_HEPMC3=ON
               ENDIF
               IF EDM4hep_native_version THEN
                  -DDD4HEP_USE_EDM4HEP=ON
               ENDIF
               -DBUILD_DOCS=OFF
               -DDD4HEP_USE_LCIO=ON
      	       -DDD4HEP_USE_TBB=ON
               -DDD4HEP_LOAD_ASSIMP=OFF
               -DDD4HEP_USE_GEANT4_UNITS=ON
               <Boost_extra_configuration>
    BUILD_COMMAND ${MAKE} 
    DEPENDS     ROOT XercesC Boost Python LCIO tbb IF <DD4hep_<NATIVE_VERSION>_geant4> STREQUAL "ON" THEN Geant4 ENDIF
    DEPENDS_OPT assimp
      IF DD4hep_native_version STREQUAL master OR DD4hep_native_version VERSION_GREATER_EQUAL 1.13 THEN hepmc3 ENDIF
      IF DD4hep_native_version STREQUAL master OR DD4hep_native_version VERSION_GREATER_EQUAL 1.15 THEN EDM4hep ENDIF
    REVISION 3
)

#---GitCondDB Package----------------------------------------------
LCG_external_package(GitCondDB       0.2.0)
LCG_user_recipe(
  GitCondDB
  GIT_REPOSITORY https://gitlab.cern.ch/lhcb/GitCondDB.git GIT_TAG <VERSION>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_CXX_STANDARD=<CMAKE_CXX_STANDARD>
  BUILD_COMMAND ${MAKE}
  DEPENDS libgit2 fmt gtest
)


#--- Pydantic -----------------------------------

#---General parameters--------------------------------------------------------------------------------------------
#set(Python_cmd ${Python_home}/bin/${PYTHON})
#set(PythonFWK_cmd ${PythonFWK_home}/Library/Frameworks/Python.framework/Versions/Current/bin/${PYTHON})
set(PYTHON python${Python_version_major})
set(PySetupOptions --root=/ --prefix=<INSTALL_DIR>)
set(MakeSitePackagesDir ${CMAKE_COMMAND} -E  make_directory <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages)

LCG_external_package(pydantic          1.9.0                                    )

LCG_user_recipe(
  pydantic
  URL ${GenURL}/pydantic-<VERSION>.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND ${PYTHON} setup.py install ${PySetupOptions}
          COMMAND ${CMAKE_SOURCE_DIR}/pyexternals/Python_postinstall.sh <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools typing_extensions
)

#---ruamel.yaml-----------------------------------------------------------------------------------
LCG_external_package(ruamel_yaml       0.17.16                                  )
LCG_User_recipe(
  ruamel_yaml
  URL ${GenURL}/ruamel.yaml-<VERSION>.tar.gz
  ENVIRONMENT RUAMEL_NO_PIP_INSTALL_CHECK=0
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND ${PYTHON} setup.py install ${PySetupOptions}
          COMMAND ${CMAKE_SOURCE_DIR}/pyexternals/Python_postinstall.sh <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)


#LCG_top_packages( GitCondDB DD4hep )
#LCG_top_packages( pydantic )
#LCG_top_packages( ruamel_yaml )
LCG_top_packages( DD4hep )


