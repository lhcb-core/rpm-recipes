#--- Version ------------------------------------------------------
set(heptools_version 97a_LHCB_3)

#---Core layer-----------------------------------------------------
include(heptools-97a)

#---Specific of this layer-----------------------------------------
# Additions
LCG_external_package(pythia8           240.lhcb4            ${MCGENPATH}/pythia8 )
LCG_external_package(powheg-box-v2     r3043.lhcb.rdynamic  ${MCGENPATH}/powheg-box-v2 )
LCG_external_package(lhapdf            6.2.3                ${MCGENPATH}/lhapdf author=6.2.3 usecxxstd=1 )
LCG_external_package(thepeg            1.9.2p1              ${MCGENPATH}/thepeg author=1.9.2 lhapdf=6.2.3 )
LCG_external_package(herwig++          2.7.1                ${MCGENPATH}/herwig++  thepeg=1.9.2.p1 )
LCG_external_package(photos++          3.56.lhcb1           ${MCGENPATH}/photos++ author=3.56)
LCG_external_package(pythia6           427.2.lhcb           ${MCGENPATH}/pythia6 author=6.4.27 hepevt=200000 )
LCG_external_package(tauola++          1.1.6b.lhcb          ${MCGENPATH}/tauola++ author=1.1.6b )
LCG_external_package(crmc              1.5.6                ${MCGENPATH}/crmc )
LCG_external_package(starlight         r300                 ${MCGENPATH}/starlight )
LCG_external_package(rivet             2.7.2b               ${MCGENPATH}/rivet yoda=1.7.7 )
LCG_external_package(madgraph5amc          2.7.2.atlas3         ${MCGENPATH}/madgraph5amc author=2.7.2 )
LCG_remove_package(herwig3)
LCG_remove_package(thep8i)

#---DD4hep Package---------------------------------------------
# Disable link with Geant4 and recompile...
set(DD4hep_v01-14-4-gf4343fc_geant4 "OFF")
LCG_external_package(DD4hep v01-14-4-gf4343fc)

#LCG_external_package(yamlcpp 0.6.3)

#LCG_top_packages( pythia8  powheg-box-v2  lhapdf  thepeg  herwig++  photos++  pythia6  tauola++  crmc  starlight  rivet madgraph5amc )
#LCG_top_packages( yamlcpp DD4hep )
LCG_top_packages( DD4hep )

