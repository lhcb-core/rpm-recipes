

%define contribName glimpse
%define contribVersion  4.18.6
%define contribPlatform Linux-x86_64
%define contribPlatformFixed Linux_x86_64
%define contribDir /opt/LHCbSoft/contrib
%define _topdir %{tmproot} 
%define tmpdir %{tmproot}
%define _tmppath %{tmproot}/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot
%undefine _disable_source_fetch

Name: %{contribName}
Version: 4.18.6
Release: 2
Vendor: Kitware
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: BSD
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: x86_64
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}
Source0: http://webglimpse.net/trial/glimpse-4.18.6.tar.gz

%description
%{contribName} %{contribVersion}


%prep
%setup

%build
./configure --prefix=${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
make

%install
make install




%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
