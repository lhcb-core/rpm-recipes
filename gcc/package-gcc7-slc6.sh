#!/bin/sh

mkdir -p /var/tmp/build/rpmbuild/BUILD
mkdir -p /var/tmp/build/tmpbuild
mkdir -p /var/tmp/build/tmp
mkdir -p /var/tmp/build/rpmbuild/SOURCES
mkdir -p /var/tmp/build/rpmbuild/RPMS

# Ignore the RPATHS checks
QA_RPATHS=$[ 0x0001|0x0002|0x0010|0x0020 ]  rpmbuild -bb gcc_7.3.0_x86_64-slc6.spec
