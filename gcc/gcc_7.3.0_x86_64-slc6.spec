
%define contribName gcc
%define contribVersion 7.3.0
%define contribPlatform x86_64-slc6
%define contribPlatformFixed x86_64_slc6
%define contribDir /opt/lcg
%define CONTRIBROOT /cvmfs/sft.cern.ch/lcg/releases

%define _topdir /var/tmp/build/rpmbuild
%define tmpdir /var/tmp/build/tmpbuild
%define _tmppath /var/tmp/build/tmp

%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: %{contribName}_%{contribVersion}_%{contribPlatformFixed}
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}_%{contribPlatformFixed}


%description
%{contribName} %{contribVersion}


%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{contribDir}
if [ $? -ne 0 ]; then
  exit $?
fi

cd ${RPM_BUILD_ROOT}%{contribDir}
mkdir -p  ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
rsync -arz %{CONTRIBROOT}/%{contribName}/%{contribVersion}/%{contribPlatform}/ ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
ln -s %{contribVersion} ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}binutils
%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
%{contribDir}/%{contribName}/%{contribVersion}binutils



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
