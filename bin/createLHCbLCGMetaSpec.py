#!/usr/bin/env python
"""
Script to create  RPM Spec files

"""
import copy
import logging
import os
import optparse
import re
import sys
from string import Template
import subprocess
from LbRelease import LCGYumInstaller
from pprint import pprint

log = logging.getLogger()
log.setLevel(logging.INFO)


# Tools to retrieve the list fo externals
#############################################################
def getExternalsList(filename):
    '''
    Gets the list of all externals needed (as from LCG 86)
    '''
    log.warning("Loading list of externals from file")
    # Cache the externals dictionary for the version...
    # Loading the metadata
    import json
    with open(filename) as f:
        data = json.load(f)
        
        # Looking for LCG version
        # We need a file like so:
        # {
        #  "heptools": {
        #    "version": 84
        #    "packages":[
        #      "AIDA",
        #      "Boost",
        #      "CLHEP",
        #      "COOL"
        #    ]
        #  }
        # }

    heptools = data["heptools"]
    lcgver = heptools["version"]
    externalsList = heptools["packages"]
    return (lcgver, externalsList)

def getRequiresList(cmtconfig, filename, siteroot="/tmp/siteroot"):

    # Setting the CMTCONFIG to the requested one
    os.environ['CMTCONFIG'] = cmtconfig

    # preparing the actual RPM install area
    installer = LCGYumInstaller.LCGYumInstaller(siteroot)

    # Retrieving the list of externals from the mentioned project
    (lcgVer, externalsList) = getExternalsList(filename)
                
    # Getting the list of packages for LCG
    log.warning("Loading list of LCG packages from YUM")
    alllcg = list(installer.list("LCG_%s_" % lcgVer))
        
    log.warning("Matching list of packages with LCG list")
    packDict = {}
    for ext in externalsList:
        packDict[ext] = None
        extName = "^LCG_%s_%s_.*_%s.*$" % (lcgVer, ext, cmtconfig.replace("-", "_"))
        for p in alllcg:
            if re.match(extName, p.rpmName()):
                packDict[ext] = p
                log.debug("%s -> %s" % ( ext, p.rpmName()))

    retlist = []
    for key, package in packDict.iteritems():
        if package is not None:
            retlist.append(package.name)
    return (lcgVer, retlist)


###############################################################################
# Class to build the SPEC itself
###############################################################################

class LHCbLCGMetaSpec(object):
    """ Class presenting the whole spec """
    def __init__(self, version, platform, requiresList, rpmroot, release = "1"):
        """ Initialize with the list of RPMs """
        self.name = "LCGLHCb"
        self.version = version
        self.platform = platform
        self.requiresList = requiresList
        self.rpmroot = rpmroot
        self.release = release
        
        # Building the build dir paths
        myroot =  rpmroot
        self.topdir = "%s/rpmbuild" % myroot
        self.tmpdir = "%s/tmpbuild" % myroot
        self.rpmtmp = "%s/tmp" % myroot
        self.srcdir = os.path.join(self.topdir, "SOURCES")
        self.rpmsdir =  os.path.join(self.topdir, "RPMS")
        self.srpmsdir =  os.path.join(self.topdir, "SRPMS")
        self.builddir =  os.path.join(self.topdir, "BUILD")
        
        # And creating them if needed
        for d in [self.srcdir, self.rpmsdir, self.srpmsdir, self.builddir]:
            if not os.path.exists(d):
                os.makedirs(d)

        self.buildroot = os.path.join(self.tmpdir, "%s-%s-%s-buildroot" % \
                                      (self.name, self.version, self.platform))
        if not os.path.exists(self.buildroot):
            os.makedirs(self.buildroot)


    
    def getHeader(self):
        """ Build the SPEC Header """
        rpm_header = Template("""
%define version $version
%define platform $platform
%define platformFixed $platformFixed

%define _topdir $topdir
%define tmpdir $tmpdir
%define _tmppath $rpmtmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_%{version}LHCb_%{platformFixed}
Version: 1.0.0
Release: $release
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

""").substitute(version=self.version,
                platform=self.platform,
                platformFixed=self.platform.replace("-","_"),
                topdir=self.topdir,
                tmpdir=self.tmpdir,
                rpmtmp=self.rpmtmp,
                release = self.release)
        return rpm_header

# RPM requirements for the whole package
#############################################################

    def getRequires(self):
        rpm_requires = ""
        for r in self.requiresList:
            rpm_requires += "Requires: %s\n" % r 
        
        return rpm_requires

# RPM Description section
#############################################################

    def getDescriptions(self):
        rpm_desc = """
%description
LCG externals %{version} %{platform} for LHCb

"""
        return rpm_desc

# RPM Common section with build
#############################################################

    def getCommon(self):
        rpm_common = """
%prep

%build

%install

%files

%post

%postun

%clean
"""
        return rpm_common

# RPM Trailer
#############################################################

    def getTrailer(self):
        rpm_trailer = """
%define date    %(echo `LC_ALL=\"C\" date +\"%a %b %d %Y\"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
"""
        return rpm_trailer


# Get the whole spec...
#############################################################

    def getSpec(self):
        """ Concatenate all the fragments """

        rpm_global = self.getHeader() \
        + self.getRequires() \
        + self.getDescriptions() \
        + self.getCommon() \
        + self.getTrailer()

        return rpm_global



# Main method
#############################################################
def usage(cmd):
    """ Prints out how to use the script... """
    cmd = os.path.basename(cmd)
    return """\n%(cmd)s [options]  platform json_externals_file

Prepare the SPEC file the LHCb Meta RPM with the list of needed externals
Call with --help for details
""" % { "cmd" : cmd }


###############################################################################
# Main method
###############################################################################
if __name__ == '__main__':
    # Setting logging 
    logging.basicConfig(stream=sys.stderr)

    # Parsing options
    parser = optparse.OptionParser(usage=usage(sys.argv[0]))
    parser.add_option('-d', '--debug',
                      dest="debug",
                      default=False,
                      action="store_true",
                      help="Show debug information")
    parser.add_option('-b', '--buildroot',
                      dest="buildroot",
                      default="/tmp",
                      action="store",
                      help="Force build root")
    parser.add_option('-o', '--output',
                      dest="output",
                      default = None,
                      action="store",
                      help="File name for the generated specfile [default output to stdout]")
    parser.add_option("-s","--siteroot",
                      help = "temporary directory where the RPMs will be installed before repackaging"
                      "[default: %default]",
                      default="/tmp/siteroot")
    parser.add_option("-r","--release",
                      help = "Release number"
                      "[default: %default]",
                      default="1")

    opts, args = parser.parse_args(sys.argv)

    print(args)
    if len(args) != 3:
        parser.error("Please specify <platform> <json_external_list>")

    if opts.debug:
        log.setLevel(logging.DEBUG)
        
    platform = args[1]
    filename = args[2]

    # Getting the list of RPMs to add
    (lcgVer, requiresList) = getRequiresList(platform, filename)
    
    print("LGC Version: %s" % lcgVer)
    print("Requires list:")
    pprint(requiresList)

    # The build root (default /tmp)
    rpmroot = opts.buildroot
        
    spec = LHCbLCGMetaSpec(lcgVer, platform, requiresList, rpmroot, opts.release)

    if opts.output:
        with open(opts.output, "w") as outputfile:
            outputfile.write(spec.getSpec())
    else:
        print spec.getSpec()
    log.info("Spec file generated")

