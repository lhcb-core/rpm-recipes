
# RPM Recipes
This repository contains various spec files and tools for rebuilding RPMs that do not fit the normal LHCb model.

* setup.sh: Setup the environment for the tools provided
* LHCbExternals: Directory containing the metadata listing the packages actually used from LCG by LHCb
* Various directories for contrib tools for which the specs are written manually (cmake, COMPAT, osc_vis..)
