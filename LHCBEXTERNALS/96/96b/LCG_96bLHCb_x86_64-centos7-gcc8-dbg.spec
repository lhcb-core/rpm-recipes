
%define version 96b
%define platform x86_64-centos7-gcc8-dbg
%define platformFixed x86_64_centos7_gcc8_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_96bLHCb_x86_64-centos7-gcc8-dbg
Version: 1.0.0
Release: 3
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_96b_AIDA_3.2.1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_Boost_1.70.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_catboost_0.9.1.1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_chardet_3.0.4_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_CLHEP_2.4.1.2_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_COOL_3_2_2_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_CORAL_3_2_1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_cppgsl_2.0.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_CppUnit_1.14.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_DD4hep_01_10_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_doxygen_1.8.15_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_eigen_3.3.7_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_fastjet_3.3.2_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_fftw_3.3.8_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_flatbuffers_1.11.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_Frontier_Client_2.8.19_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_gdb_8.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_graphviz_2.40.1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_GSL_2.5_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_HepMC_2.06.10_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_HepPDT_2.06.01_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_idna_2.8_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_ipython_genutils_0.2.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_lapack_3.8.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_libgit2_0.28.2_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_libunwind_1.3.1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_libxml2_2.9.9_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_lxml_4.3.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_matplotlib_2.2.4_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_mpmath_1.1.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_networkx_2.2_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_neurobayes_expert_3.7.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_oracle_18.3.0.0.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_pathos_0.2.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_pyanalysis_2.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_pygraphics_2.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_pyqt5_5.12.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_Python_2.7.16_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_pytools_2.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_QMtest_2.4.1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_Qt5_5.12.4_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_rangev3_0.5.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_RELAX_root6_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_ROOT_6.18.04_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_sqlite_3280000_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_tbb_2019_U7_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_tensorflow_1.14.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_Vc_1.4.1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_vdt_0.4.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_veccore_0.6.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_vectorclass_1.30p1_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_wcwidth_0.1.7_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_XercesC_3.1.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_xgboost_0.90_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_xqilla_2.3.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_xrootd_4.10.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_packaging_19.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_fmt_5.3.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_six_1.12.0_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_coverage_4.5.3_x86_64_centos7_gcc8_dbg
Requires: LCG_96b_jemalloc_5.2.0_x86_64_centos7_gcc8_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
