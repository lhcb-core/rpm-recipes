
%define version 96
%define platform x86_64-centos7-clang8-dbg
%define platformFixed x86_64_centos7_clang8_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_96LHCbGenerators_x86_64-centos7-clang8-dbg
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_96_alpgen_2.1.4_x86_64_centos7_clang8_dbg
Requires: LCG_96_herwig++_2.7.1_x86_64_centos7_clang8_dbg
Requires: LCG_96_hijing_1.383bs.2_x86_64_centos7_clang8_dbg
Requires: LCG_96_lhapdf_6.2.3_x86_64_centos7_clang8_dbg
Requires: LCG_96_photos++_3.56_x86_64_centos7_clang8_dbg
Requires: LCG_96_powheg-box-v2_r3043.lhcb_x86_64_centos7_clang8_dbg
Requires: LCG_96_pythia6_427.2_x86_64_centos7_clang8_dbg
Requires: LCG_96_pythia8_240_x86_64_centos7_clang8_dbg
Requires: LCG_96_rivet_2.7.2b_x86_64_centos7_clang8_dbg
Requires: LCG_96_tauola++_1.1.6b.lhcb_x86_64_centos7_clang8_dbg
Requires: LCG_96_thepeg_2.1.5_x86_64_centos7_clang8_dbg
Requires: LCG_96_crmc_1.5.6_x86_64_centos7_clang8_dbg
Requires: LCG_96_yoda_1.7.5_x86_64_centos7_clang8_dbg
Requires: LCG_96_starlight_r300_x86_64_centos7_clang8_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
