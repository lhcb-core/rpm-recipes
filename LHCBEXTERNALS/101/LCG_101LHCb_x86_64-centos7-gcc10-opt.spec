
%define version 101
%define platform x86_64-centos7-gcc10-opt
%define platformFixed x86_64_centos7_gcc10_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_101LHCb_x86_64-centos7-gcc10-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_101_AIDA_3.2.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_Boost_1.77.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_catboost_0.26.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_chardet_3.0.4_x86_64_centos7_gcc10_opt
Requires: LCG_101_clhep_2.4.4.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_cppgsl_3.1.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_CppUnit_1.14.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_doxygen_1.8.18_x86_64_centos7_gcc10_opt
Requires: LCG_101_eigen_3.3.7_x86_64_centos7_gcc10_opt
Requires: LCG_101_fastjet_3.4.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_fftw_3.3.8_x86_64_centos7_gcc10_opt
Requires: LCG_101_flatbuffers_1.12.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_gdb_10.2_x86_64_centos7_gcc10_opt
Requires: LCG_101_graphviz_py_0.11.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_graphviz_2.40.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_GSL_2.7_x86_64_centos7_gcc10_opt
Requires: LCG_101_HepMC_2.06.11_x86_64_centos7_gcc10_opt
Requires: LCG_101_HepPDT_2.06.01_x86_64_centos7_gcc10_opt
Requires: LCG_101_idna_3.2_x86_64_centos7_gcc10_opt
Requires: LCG_101_ipython_genutils_0.2.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_ipython_7.25.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_blas_0.3.17.openblas_x86_64_centos7_gcc10_opt
Requires: LCG_101_libgit2_1.1.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_libunwind_1.3.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_libxml2_2.9.10_x86_64_centos7_gcc10_opt
Requires: LCG_101_lxml_4.6.2_x86_64_centos7_gcc10_opt
Requires: LCG_101_matplotlib_3.4.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_matplotlib_inline_0.1.2_x86_64_centos7_gcc10_opt
Requires: LCG_101_mpmath_1.2.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_networkx_2.5.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_oracle_19.11.0.0.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_pathos_0.2.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_Python_3.9.6_x86_64_centos7_gcc10_opt
Requires: LCG_101_rangev3_0.11.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_RELAX_root6_x86_64_centos7_gcc10_opt
Requires: LCG_101_ROOT_6.24.06_x86_64_centos7_gcc10_opt
Requires: LCG_101_sqlite_3320300_x86_64_centos7_gcc10_opt
Requires: LCG_101_tbb_2020_U2_x86_64_centos7_gcc10_opt
Requires: LCG_101_gperftools_2.9.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_tensorflow_estimator_2.5.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_tensorflow_2.5.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_tensorflow_model_optimization_0.6.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_Vc_1.4.2_x86_64_centos7_gcc10_opt
Requires: LCG_101_vdt_0.4.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_veccore_0.7.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_vectorclass_2.01.02_x86_64_centos7_gcc10_opt
Requires: LCG_101_wcwidth_0.2.5_x86_64_centos7_gcc10_opt
Requires: LCG_101_XercesC_3.2.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_xgboost_0.90_x86_64_centos7_gcc10_opt
Requires: LCG_101_xqilla_2.3.4_x86_64_centos7_gcc10_opt
Requires: LCG_101_xrootd_5.3.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_packaging_21.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_fmt_7.1.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_six_1.16.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_coverage_4.5.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_jemalloc_5.2.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_future_0.17.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_jsonmcpp_3.9.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_spdlog_1.5.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_gperftools_2.9.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_sortedcontainers_2.1.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_yamlcpp_0.6.3_x86_64_centos7_gcc10_opt
Requires: LCG_101_wrapt_1.11.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_pytest_runner_5.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_pytest_6.1.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_click_7.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_cachetools_3.1.1_x86_64_centos7_gcc10_opt
Requires: LCG_101_torch_1.9.0_x86_64_centos7_gcc10_opt
Requires: LCG_101_sympy_1.8_x86_64_centos7_gcc10_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
