
%define version 104_LHCB_7
%define platform x86_64-el9-gcc13-opt
%define platformFixed x86_64_el9_gcc13_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_104_LHCB_7LHCb_x86_64-el9-gcc13-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_x86_64_el9_gcc13_opt
Requires: Boost-fbfc9_1.82.0_x86_64_el9_gcc13_opt
Requires: Catch2-31748_2.13.9_x86_64_el9_gcc13_opt
Requires: CppUnit-b79b3_1.14.0_x86_64_el9_gcc13_opt
Requires: DD4hep-e22f9_01.26_x86_64_el9_gcc13_opt
Requires: GSL-30ba4_2.7_x86_64_el9_gcc13_opt
Requires: GitCondDB-40070_0.2.2_x86_64_el9_gcc13_opt
Requires: HepMC-d5a39_2.06.11_x86_64_el9_gcc13_opt
Requires: HepPDT-dba76_2.06.01_x86_64_el9_gcc13_opt
Requires: Jinja2-7c1ef_3.1.2_x86_64_el9_gcc13_opt
Requires: PyYAML-139f2_6.0_x86_64_el9_gcc13_opt
Requires: Python-9a1bc_3.9.12_x86_64_el9_gcc13_opt
Requires: RELAX-072c8_6.0.1_x86_64_el9_gcc13_opt
Requires: ROOT-740f6_6.28.04_x86_64_el9_gcc13_opt
Requires: Vc-eba14_1.4.3p1_x86_64_el9_gcc13_opt
Requires: XercesC-9e637_3.2.4_x86_64_el9_gcc13_opt
Requires: blas-c07f1_0.3.20.openblas_x86_64_el9_gcc13_opt
Requires: cachetools-c5642_5.3.1_x86_64_el9_gcc13_opt
Requires: catboost-d6adc_1.2_x86_64_el9_gcc13_opt
Requires: chardet-85354_3.0.4_x86_64_el9_gcc13_opt
Requires: clhep-2ef70_2.4.6.4_x86_64_el9_gcc13_opt
Requires: click-07a71_8.0.3_x86_64_el9_gcc13_opt
Requires: coverage-cbd70_6.3.2_x86_64_el9_gcc13_opt
Requires: cppgsl-7a1b6_3.1.0_x86_64_el9_gcc13_opt
Requires: crmc-665f4_1.8.0.lhcb_x86_64_el9_gcc13_opt
Requires: doxygen-6cb62_1.8.18_x86_64_el9_gcc13_opt
Requires: eigen-6ce89_3.4.0_x86_64_el9_gcc13_opt
Requires: fastjet-5af57_3.4.1_x86_64_el9_gcc13_opt
Requires: fftw-33229_3.3.10_x86_64_el9_gcc13_opt
Requires: flatbuffers-fbe40_2.0.8_x86_64_el9_gcc13_opt
Requires: fmt-f6609_10.0.0_x86_64_el9_gcc13_opt
Requires: future-4d201_0.18.3_x86_64_el9_gcc13_opt
Requires: gdb-de38f_13.2_x86_64_el9_gcc13_opt
Requires: gperftools-9ac98_2.9.1_x86_64_el9_gcc13_opt
Requires: graphviz-d9648_8.0.5_x86_64_el9_gcc13_opt
Requires: herwig3-7faa8_7.2.1_x86_64_el9_gcc13_opt
Requires: idna-e6055_3.2_x86_64_el9_gcc13_opt
Requires: ipython-4e723_7.25.0_x86_64_el9_gcc13_opt
Requires: jemalloc-5a247_5.3.0_x86_64_el9_gcc13_opt
Requires: jsonmcpp-f26c3_3.10.5_x86_64_el9_gcc13_opt
Requires: lhapdf-79828_6.2.3_x86_64_el9_gcc13_opt
Requires: libgit2-98d83_1.1.1_x86_64_el9_gcc13_opt
Requires: libunwind-2fa0a_1.3.1_x86_64_el9_gcc13_opt
Requires: lxml-8cc17_4.6.2_x86_64_el9_gcc13_opt
Requires: madgraph5amc-32303_2.9.3.atlas1_x86_64_el9_gcc13_opt
Requires: matplotlib-1d4c0_3.7.1_x86_64_el9_gcc13_opt
Requires: mpmath-7178a_1.2.1_x86_64_el9_gcc13_opt
Requires: networkx-429e3_2.8.4_x86_64_el9_gcc13_opt
Requires: onnxruntime-8b3a0_1.15.1_x86_64_el9_gcc13_opt
Requires: oracle-ea4bc_19.19.0.0.0_x86_64_el9_gcc13_opt
Requires: packaging-8f5dc_23.1_x86_64_el9_gcc13_opt
Requires: pathos-ab978_0.2.3_x86_64_el9_gcc13_opt
Requires: photos++-29b42_3.56.lhcb1_x86_64_el9_gcc13_opt
Requires: powheg-box-v2-0f8a9_r3744.lhcb4.rdynamic_x86_64_el9_gcc13_opt
Requires: pydantic-2f881_1.10.9_x86_64_el9_gcc13_opt
Requires: pydot-107bc_1.4.1_x86_64_el9_gcc13_opt
Requires: pytest-cc524_7.4.0_x86_64_el9_gcc13_opt
Requires: pytest_cov-04781_3.0.0_x86_64_el9_gcc13_opt
Requires: pythia6-94243_427.2.lhcb_x86_64_el9_gcc13_opt
Requires: pythia8-81c49_244.lhcb4_x86_64_el9_gcc13_opt
Requires: pyzmq-1c4ae_22.1.0_x86_64_el9_gcc13_opt
Requires: rangev3-79ab4_0.11.0_x86_64_el9_gcc13_opt
Requires: rivet-4ae18_3.1.4_x86_64_el9_gcc13_opt
Requires: ruamel_yaml-d81fa_0.17.31_x86_64_el9_gcc13_opt
Requires: six-7d0d9_1.16.0_x86_64_el9_gcc13_opt
Requires: sortedcontainers-f9052_2.1.0_x86_64_el9_gcc13_opt
Requires: spdlog-15a38_1.9.2_x86_64_el9_gcc13_opt
Requires: sqlite-3c47f_3320300_x86_64_el9_gcc13_opt
Requires: starlight-62bcd_r313_x86_64_el9_gcc13_opt
Requires: sympy-b3036_1.8_x86_64_el9_gcc13_opt
Requires: tauola++-d23c6_1.1.6b.lhcb_x86_64_el9_gcc13_opt
Requires: tbb-2e3ca_2020_U2_x86_64_el9_gcc13_opt
Requires: tensorflow-decca_2.12.0_x86_64_el9_gcc13_opt
Requires: thepeg-f824b_2.2.1_x86_64_el9_gcc13_opt
Requires: torch-8f4d1_2.0.1p1_x86_64_el9_gcc13_opt
Requires: vdt-260e4_0.4.4_x86_64_el9_gcc13_opt
Requires: veccore-18dd8_0.8.1_x86_64_el9_gcc13_opt
Requires: vectorclass-cdfdb_2.01.02_x86_64_el9_gcc13_opt
Requires: wcwidth-e6a86_0.2.5_x86_64_el9_gcc13_opt
Requires: wrapt-82b73_1.14.1_x86_64_el9_gcc13_opt
Requires: xgboost-910f6_0.90_x86_64_el9_gcc13_opt
Requires: xqilla-f303e_2.3.4_x86_64_el9_gcc13_opt
Requires: xrootd-6df42_5.5.4_x86_64_el9_gcc13_opt
Requires: yamlcpp-d05b2_0.6.3_x86_64_el9_gcc13_opt
Requires: yoda-f916b_1.9.0_x86_64_el9_gcc13_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
