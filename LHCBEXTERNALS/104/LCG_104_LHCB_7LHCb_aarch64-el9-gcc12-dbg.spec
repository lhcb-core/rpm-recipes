
%define version 104_LHCB_7
%define platform aarch64-el9-gcc12-dbg
%define platformFixed aarch64_el9_gcc12_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_104_LHCB_7LHCb_aarch64-el9-gcc12-dbg
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_aarch64_el9_gcc12_dbg
Requires: Boost-9c64a_1.82.0_aarch64_el9_gcc12_dbg
Requires: Catch2-31748_2.13.9_aarch64_el9_gcc12_dbg
Requires: CppUnit-b79b3_1.14.0_aarch64_el9_gcc12_dbg
Requires: DD4hep-251fc_01.26_aarch64_el9_gcc12_dbg
Requires: GSL-30ba4_2.7_aarch64_el9_gcc12_dbg
Requires: GitCondDB-40070_0.2.2_aarch64_el9_gcc12_dbg
Requires: HepMC-d5a39_2.06.11_aarch64_el9_gcc12_dbg
Requires: HepPDT-dba76_2.06.01_aarch64_el9_gcc12_dbg
Requires: Jinja2-7c1ef_3.1.2_aarch64_el9_gcc12_dbg
Requires: PyYAML-139f2_6.0_aarch64_el9_gcc12_dbg
Requires: Python-9a1bc_3.9.12_aarch64_el9_gcc12_dbg
Requires: RELAX-9e7b3_6.0.1_aarch64_el9_gcc12_dbg
Requires: ROOT-f22ce_6.28.04_aarch64_el9_gcc12_dbg
Requires: Vc-eba14_1.4.3p1_aarch64_el9_gcc12_dbg
Requires: XercesC-9e637_3.2.4_aarch64_el9_gcc12_dbg
Requires: blas-e5f0c_0.3.17.openblas_aarch64_el9_gcc12_dbg
Requires: cachetools-c5642_5.3.1_aarch64_el9_gcc12_dbg
Requires: catboost-d6adc_1.2_aarch64_el9_gcc12_dbg
Requires: chardet-85354_3.0.4_aarch64_el9_gcc12_dbg
Requires: clhep-2ef70_2.4.6.4_aarch64_el9_gcc12_dbg
Requires: click-07a71_8.0.3_aarch64_el9_gcc12_dbg
Requires: coverage-cbd70_6.3.2_aarch64_el9_gcc12_dbg
Requires: cppgsl-7a1b6_3.1.0_aarch64_el9_gcc12_dbg
Requires: crmc-8795a_1.8.0.lhcb_aarch64_el9_gcc12_dbg
Requires: doxygen-6cb62_1.8.18_aarch64_el9_gcc12_dbg
Requires: eigen-6ce89_3.4.0_aarch64_el9_gcc12_dbg
Requires: fastjet-5af57_3.4.1_aarch64_el9_gcc12_dbg
Requires: fftw-33229_3.3.10_aarch64_el9_gcc12_dbg
Requires: flatbuffers-d94e9_2.0.8_aarch64_el9_gcc12_dbg
Requires: fmt-f6609_10.0.0_aarch64_el9_gcc12_dbg
Requires: future-4d201_0.18.3_aarch64_el9_gcc12_dbg
Requires: gdb-de38f_13.2_aarch64_el9_gcc12_dbg
Requires: gperftools-9ac98_2.9.1_aarch64_el9_gcc12_dbg
Requires: graphviz-d9648_8.0.5_aarch64_el9_gcc12_dbg
Requires: herwig3-925a9_7.2.1_aarch64_el9_gcc12_dbg
Requires: idna-e6055_3.2_aarch64_el9_gcc12_dbg
Requires: ipython-4e723_7.25.0_aarch64_el9_gcc12_dbg
Requires: jemalloc-5a247_5.3.0_aarch64_el9_gcc12_dbg
Requires: jsonmcpp-f26c3_3.10.5_aarch64_el9_gcc12_dbg
Requires: lhapdf-79828_6.2.3_aarch64_el9_gcc12_dbg
Requires: libgit2-98d83_1.1.1_aarch64_el9_gcc12_dbg
Requires: libunwind-2fa0a_1.3.1_aarch64_el9_gcc12_dbg
Requires: lxml-8cc17_4.6.2_aarch64_el9_gcc12_dbg
Requires: madgraph5amc-32303_2.9.3.atlas1_aarch64_el9_gcc12_dbg
Requires: matplotlib-25c9d_3.7.1_aarch64_el9_gcc12_dbg
Requires: mpmath-7178a_1.2.1_aarch64_el9_gcc12_dbg
Requires: networkx-f2735_2.8.4_aarch64_el9_gcc12_dbg
Requires: oracle-da6b7_19.10.0.0.0_aarch64_el9_gcc12_dbg
Requires: packaging-8f5dc_23.1_aarch64_el9_gcc12_dbg
Requires: pathos-ab978_0.2.3_aarch64_el9_gcc12_dbg
Requires: photos++-29b42_3.56.lhcb1_aarch64_el9_gcc12_dbg
Requires: powheg-box-v2-0f8a9_r3744.lhcb4.rdynamic_aarch64_el9_gcc12_dbg
Requires: pydantic-2f881_1.10.9_aarch64_el9_gcc12_dbg
Requires: pydot-107bc_1.4.1_aarch64_el9_gcc12_dbg
Requires: pytest-cc524_7.4.0_aarch64_el9_gcc12_dbg
Requires: pytest_cov-04781_3.0.0_aarch64_el9_gcc12_dbg
Requires: pythia6-94243_427.2.lhcb_aarch64_el9_gcc12_dbg
Requires: pythia8-81c49_244.lhcb4_aarch64_el9_gcc12_dbg
Requires: pyzmq-1c4ae_22.1.0_aarch64_el9_gcc12_dbg
Requires: rangev3-79ab4_0.11.0_aarch64_el9_gcc12_dbg
Requires: rivet-7c692_3.1.4_aarch64_el9_gcc12_dbg
Requires: ruamel_yaml-d81fa_0.17.31_aarch64_el9_gcc12_dbg
Requires: six-7d0d9_1.16.0_aarch64_el9_gcc12_dbg
Requires: sortedcontainers-f9052_2.1.0_aarch64_el9_gcc12_dbg
Requires: spdlog-15a38_1.9.2_aarch64_el9_gcc12_dbg
Requires: sqlite-3c47f_3320300_aarch64_el9_gcc12_dbg
Requires: starlight-62bcd_r313_aarch64_el9_gcc12_dbg
Requires: sympy-b3036_1.8_aarch64_el9_gcc12_dbg
Requires: tauola++-d23c6_1.1.6b.lhcb_aarch64_el9_gcc12_dbg
Requires: tbb-2e3ca_2020_U2_aarch64_el9_gcc12_dbg
Requires: thepeg-ebbb1_2.2.1_aarch64_el9_gcc12_dbg
Requires: vdt-260e4_0.4.4_aarch64_el9_gcc12_dbg
Requires: veccore-18dd8_0.8.1_aarch64_el9_gcc12_dbg
Requires: vectorclass-cdfdb_2.01.02_aarch64_el9_gcc12_dbg
Requires: wcwidth-e6a86_0.2.5_aarch64_el9_gcc12_dbg
Requires: wrapt-82b73_1.14.1_aarch64_el9_gcc12_dbg
Requires: xgboost-05944_0.90_aarch64_el9_gcc12_dbg
Requires: xrootd-7b80d_5.5.4_aarch64_el9_gcc12_dbg
Requires: yamlcpp-d05b2_0.6.3_aarch64_el9_gcc12_dbg
Requires: yoda-3eea4_1.9.0_aarch64_el9_gcc12_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
