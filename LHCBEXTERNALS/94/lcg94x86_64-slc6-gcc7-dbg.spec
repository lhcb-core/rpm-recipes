
%define version 94
%define platform x86_64-slc6-gcc7-dbg
%define platformFixed x86_64_slc6_gcc7_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_%{version}LHCb_%{platformFixed}
Version: 1.0.0
Release: 2
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_94_libgit2_0.27.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_sqlite_3210000_x86_64_slc6_gcc7_dbg
Requires: LCG_94_xgboost_0.71_x86_64_slc6_gcc7_dbg
Requires: LCG_94_eigen_3.2.9_x86_64_slc6_gcc7_dbg
Requires: LCG_94_doxygen_1.8.11_x86_64_slc6_gcc7_dbg
Requires: LCG_94_Frontier_Client_2.8.19_x86_64_slc6_gcc7_dbg
Requires: LCG_94_libxml2_2.9.7_x86_64_slc6_gcc7_dbg
Requires: LCG_94_AIDA_3.2.1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_rangev3_0.3.6_x86_64_slc6_gcc7_dbg
Requires: LCG_94_HepMC_2.06.09_x86_64_slc6_gcc7_dbg
Requires: LCG_94_graphviz_2.28.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_gdb_7.12.1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_pyanalysis_2.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_Boost_1.66.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_GSL_2.5_x86_64_slc6_gcc7_dbg
Requires: LCG_94_pygraphics_2.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_tbb_2018_U1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_CLHEP_2.4.1.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_lapack_3.5.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_Python_2.7.15_x86_64_slc6_gcc7_dbg
Requires: LCG_94_lxml_4.2.3_x86_64_slc6_gcc7_dbg
Requires: LCG_94_pytools_2.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_HepPDT_2.06.01_x86_64_slc6_gcc7_dbg
Requires: LCG_94_fastjet_3.3.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_tensorflow_1.8.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_ROOT_6.14.04_x86_64_slc6_gcc7_dbg
Requires: LCG_94_veccore_0.4.2_x86_64_slc6_gcc7_dbg
Requires: LCG_94_vectorclass_1.30_x86_64_slc6_gcc7_dbg
Requires: LCG_94_XercesC_3.1.3_x86_64_slc6_gcc7_dbg
Requires: LCG_94_Qt5_5.11.1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_CORAL_3_2_0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_flatbuffers_1.8.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_xrootd_python_0.3.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_libunwind_5c2cade_x86_64_slc6_gcc7_dbg
Requires: LCG_94_DD4hep_01_08_x86_64_slc6_gcc7_dbg
Requires: LCG_94_ipython_genutils_0.2.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_neurobayes_expert_3.7.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_CppUnit_1.12.1_p1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_COOL_3_2_0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_Vc_1.3.2_x86_64_slc6_gcc7_dbg
Requires: LCG_94_vdt_0.3.9_x86_64_slc6_gcc7_dbg
Requires: LCG_94_RELAX_root6_x86_64_slc6_gcc7_dbg
Requires: LCG_94_QMtest_2.4.1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_fftw_3.3.4_x86_64_slc6_gcc7_dbg
Requires: LCG_94_wcwidth_0.1.7_x86_64_slc6_gcc7_dbg
Requires: LCG_94_xqilla_2.3.3_x86_64_slc6_gcc7_dbg
Requires: LCG_94_catboost_0.9.1.1_x86_64_slc6_gcc7_dbg
Requires: LCG_94_mpmath_1.0.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_chardet_3.0.4_x86_64_slc6_gcc7_dbg
Requires: LCG_94_oracle_11.2.0.3.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_cppgsl_1.0.0_x86_64_slc6_gcc7_dbg
Requires: LCG_94_idna_2.7_x86_64_slc6_gcc7_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
