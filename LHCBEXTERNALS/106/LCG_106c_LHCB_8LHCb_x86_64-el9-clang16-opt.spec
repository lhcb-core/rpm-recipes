
%define version 106c_LHCB_8
%define platform x86_64-el9-clang16-opt
%define platformFixed x86_64_el9_clang16_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_106c_LHCB_8LHCb_x86_64-el9-clang16-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_x86_64_el9_clang16_opt
Requires: Boost-4a4ec_1.86.0_x86_64_el9_clang16_opt
Requires: Catch2-c2e00_2.13.9_x86_64_el9_clang16_opt
Requires: CppUnit-b79b3_1.14.0_x86_64_el9_clang16_opt
Requires: DD4hep-727d6_01.31_x86_64_el9_clang16_opt
Requires: GSL-30ba4_2.7_x86_64_el9_clang16_opt
Requires: GitCondDB-8a285_0.2.2_x86_64_el9_clang16_opt
Requires: HepMC-d5a39_2.06.11_x86_64_el9_clang16_opt
Requires: HepPDT-dba76_2.06.01_x86_64_el9_clang16_opt
Requires: Jinja2-eef38_3.1.2_x86_64_el9_clang16_opt
Requires: PyYAML-6924b_6.0.1_x86_64_el9_clang16_opt
Requires: Python-2924c_3.11.9_x86_64_el9_clang16_opt
Requires: RELAX-43d48_6.1.2_x86_64_el9_clang16_opt
Requires: ROOT-73580_6.32.10_x86_64_el9_clang16_opt
Requires: Vc-341bf_1.4.5_x86_64_el9_clang16_opt
Requires: XercesC-9e637_3.2.4_x86_64_el9_clang16_opt
Requires: blas-c07f1_0.3.20.openblas_x86_64_el9_clang16_opt
Requires: cachetools-9a707_5.3.1_x86_64_el9_clang16_opt
Requires: catboost-d6adc_1.2_x86_64_el9_clang16_opt
Requires: chardet-76e32_3.0.4_x86_64_el9_clang16_opt
Requires: clhep-b7a7d_2.4.7.1_x86_64_el9_clang16_opt
Requires: click-ea84d_8.1.7_x86_64_el9_clang16_opt
Requires: coverage-b64ca_7.4.1_x86_64_el9_clang16_opt
Requires: cppgsl-7a1b6_3.1.0_x86_64_el9_clang16_opt
Requires: crmc-04cc4_2.0.1p5_x86_64_el9_clang16_opt
Requires: doxygen-b94e2_1.12.0_x86_64_el9_clang16_opt
Requires: eigen-6ce89_3.4.0_x86_64_el9_clang16_opt
Requires: fastjet-5af57_3.4.1_x86_64_el9_clang16_opt
Requires: fftw-33229_3.3.10_x86_64_el9_clang16_opt
Requires: fjcontrib-fb6f5_1.052_x86_64_el9_clang16_opt
Requires: flatbuffers-fe1aa_23.5.26_x86_64_el9_clang16_opt
Requires: fmt-96b03_10.2.1_x86_64_el9_clang16_opt
Requires: future-44753_0.18.3_x86_64_el9_clang16_opt
Requires: gdb-047f5_15.1_x86_64_el9_clang16_opt
Requires: gperftools-8ed9f_2.15_x86_64_el9_clang16_opt
Requires: graphviz-6d9da_8.0.5_x86_64_el9_clang16_opt
Requires: herwig3-aed64_7.2.3p2_x86_64_el9_clang16_opt
Requires: idna-7c7b4_3.2_x86_64_el9_clang16_opt
Requires: ipython-0f4da_7.25.0_x86_64_el9_clang16_opt
Requires: jemalloc-5a247_5.3.0_x86_64_el9_clang16_opt
Requires: jsonmcpp-0cf2d_3.11.3_x86_64_el9_clang16_opt
Requires: lhapdf-52323_6.2.3p1_x86_64_el9_clang16_opt
Requires: libgit2-35190_1.1.1_x86_64_el9_clang16_opt
Requires: libunwind-68830_1.5.0_x86_64_el9_clang16_opt
Requires: libxml2-87838_2.10.4_x86_64_el9_clang16_opt
Requires: lxml-172d0_5.2.2_x86_64_el9_clang16_opt
Requires: madgraph5amc-391df_2.9.3.atlas1_x86_64_el9_clang16_opt
Requires: matplotlib-1217d_3.8.3_x86_64_el9_clang16_opt
Requires: mpmath-2438e_1.2.1_x86_64_el9_clang16_opt
Requires: networkx-e549d_3.2.1_x86_64_el9_clang16_opt
Requires: onnxruntime-b7091_1.20.1_x86_64_el9_clang16_opt
Requires: oracle-ea4bc_19.19.0.0.0_x86_64_el9_clang16_opt
Requires: packaging-b0801_23.2_x86_64_el9_clang16_opt
Requires: pandas-586c3_2.2.2_x86_64_el9_clang16_opt
Requires: pathos-57475_0.2.3_x86_64_el9_clang16_opt
Requires: photos++-6af5c_3.56.lhcb1_x86_64_el9_clang16_opt
Requires: pydantic-28a64_2.8.2_x86_64_el9_clang16_opt
Requires: pydot-a55e4_1.4.1_x86_64_el9_clang16_opt
Requires: pyeda-0516a_0.29.0_x86_64_el9_clang16_opt
Requires: pytest-a3a3a_8.3.2_x86_64_el9_clang16_opt
Requires: pytest_cov-4de32_4.1.0_x86_64_el9_clang16_opt
Requires: pythia6-94243_427.2.lhcb_x86_64_el9_clang16_opt
Requires: pythia8-9b067_244.lhcb4_x86_64_el9_clang16_opt
Requires: pyzmq-8c765_22.1.0_x86_64_el9_clang16_opt
Requires: rangev3-79ab4_0.11.0_x86_64_el9_clang16_opt
Requires: rapidyaml-ef0a1_0.70.0_x86_64_el9_clang16_opt
Requires: rivet-dc622_3.1.10_x86_64_el9_clang16_opt
Requires: ruamel_yaml-4b24f_0.17.31_x86_64_el9_clang16_opt
Requires: shapely-3ef0c_2.0.5_x86_64_el9_clang16_opt
Requires: six-e0b93_1.16.0_x86_64_el9_clang16_opt
Requires: sortedcontainers-819be_2.1.0_x86_64_el9_clang16_opt
Requires: spdlog-15a38_1.9.2_x86_64_el9_clang16_opt
Requires: sqlite-3c47f_3320300_x86_64_el9_clang16_opt
Requires: starlight-62bcd_r313_x86_64_el9_clang16_opt
Requires: sympy-368cb_1.12_x86_64_el9_clang16_opt
Requires: tauola++-00d6d_1.1.6b.lhcb_x86_64_el9_clang16_opt
Requires: tbb-2a247_2021.10.0_x86_64_el9_clang16_opt
Requires: tensorflow-1e83e_2.16.1_x86_64_el9_clang16_opt
Requires: thepeg-032aa_2.2.3_x86_64_el9_clang16_opt
Requires: torch-b347a_2.3.1_x86_64_el9_clang16_opt
Requires: valgrind-24262_3.23.0_x86_64_el9_clang16_opt
Requires: vdt-48c41_0.4.4_x86_64_el9_clang16_opt
Requires: veccore-f2b7a_0.8.1_x86_64_el9_clang16_opt
Requires: vectorclass-cdfdb_2.01.02_x86_64_el9_clang16_opt
Requires: wcwidth-e8846_0.2.5_x86_64_el9_clang16_opt
Requires: wrapt-fd47c_1.14.1_x86_64_el9_clang16_opt
Requires: xgboost-58e3e_0.90_x86_64_el9_clang16_opt
Requires: xqilla-f303e_2.3.4_x86_64_el9_clang16_opt
Requires: xrootd-46b79_5.7.2_x86_64_el9_clang16_opt
Requires: yamlcpp-d05b2_0.6.3_x86_64_el9_clang16_opt
Requires: yoda-1a93d_1.9.10_x86_64_el9_clang16_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
