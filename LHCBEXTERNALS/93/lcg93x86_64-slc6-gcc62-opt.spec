
%define version 93
%define platform x86_64-slc6-gcc62-opt
%define platformFixed x86_64_slc6_gcc62_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_%{version}LHCb_%{platformFixed}
Version: 1.0.0
Release: 5
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_93_libgit2_0.26.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_sqlite_3210000_x86_64_slc6_gcc62_opt
Requires: LCG_93_xgboost_0.60_x86_64_slc6_gcc62_opt
Requires: LCG_93_eigen_3.2.9_x86_64_slc6_gcc62_opt
Requires: LCG_93_doxygen_1.8.11_x86_64_slc6_gcc62_opt
Requires: LCG_93_Frontier_Client_2.8.19_x86_64_slc6_gcc62_opt
Requires: LCG_93_libxml2_2.9.7_x86_64_slc6_gcc62_opt
Requires: LCG_93_AIDA_3.2.1_x86_64_slc6_gcc62_opt
Requires: LCG_93_rangev3_0.3.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_HepMC_2.06.09_x86_64_slc6_gcc62_opt
Requires: LCG_93_graphviz_2.28.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_gdb_7.12.1_x86_64_slc6_gcc62_opt
Requires: LCG_93_pyanalysis_2.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_Boost_1.66.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_GSL_2.1_x86_64_slc6_gcc62_opt
Requires: LCG_93_pygraphics_2.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_tbb_2018_U1_x86_64_slc6_gcc62_opt
Requires: LCG_93_CLHEP_2.4.0.1_x86_64_slc6_gcc62_opt
Requires: LCG_93_lapack_3.5.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_Python_2.7.13_x86_64_slc6_gcc62_opt
Requires: LCG_93_lxml_4.1.1_x86_64_slc6_gcc62_opt
Requires: LCG_93_pytools_2.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_HepPDT_2.06.01_x86_64_slc6_gcc62_opt
Requires: LCG_93_fastjet_3.3.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_tensorflow_1.4.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_ROOT_6.12.06_x86_64_slc6_gcc62_opt
Requires: LCG_93_veccore_0.4.2_x86_64_slc6_gcc62_opt
Requires: LCG_93_vectorclass_1.30_x86_64_slc6_gcc62_opt
Requires: LCG_93_XercesC_3.1.3_x86_64_slc6_gcc62_opt
Requires: LCG_93_Qt_4.8.7_x86_64_slc6_gcc62_opt
Requires: LCG_93_Qt5_5.9.2_x86_64_slc6_gcc62_opt
Requires: LCG_93_CORAL_3_2_0_x86_64_slc6_gcc62_opt
Requires: LCG_93_flatbuffers_1.8.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_xrootd_python_0.3.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_libunwind_5c2cade_x86_64_slc6_gcc62_opt
Requires: LCG_93_ipython_genutils_0.1.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_neurobayes_expert_3.7.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_CppUnit_1.12.1_p1_x86_64_slc6_gcc62_opt
Requires: LCG_93_COOL_3_2_0_x86_64_slc6_gcc62_opt
Requires: LCG_93_Vc_1.3.2_x86_64_slc6_gcc62_opt
Requires: LCG_93_vdt_0.3.9_x86_64_slc6_gcc62_opt
Requires: LCG_93_RELAX_root6_x86_64_slc6_gcc62_opt
Requires: LCG_93_QMtest_2.4.1_x86_64_slc6_gcc62_opt
Requires: LCG_93_fftw_3.3.4_x86_64_slc6_gcc62_opt
Requires: LCG_93_wcwidth_0.1.7_x86_64_slc6_gcc62_opt
Requires: LCG_93_xqilla_2.3.3_x86_64_slc6_gcc62_opt
Requires: LCG_93_catboost_0.5_x86_64_slc6_gcc62_opt
Requires: LCG_93_chardet_3.0.4_x86_64_slc6_gcc62_opt
Requires: LCG_93_oracle_11.2.0.3.0_x86_64_slc6_gcc62_opt
Requires: LCG_93_cppgsl_b07383ea_x86_64_slc6_gcc62_opt
Requires: LCG_93_idna_2.6_x86_64_slc6_gcc62_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
