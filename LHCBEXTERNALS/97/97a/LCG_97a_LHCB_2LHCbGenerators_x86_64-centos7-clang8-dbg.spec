
%define version 97a_LHCB_2
%define platform x86_64-centos7-clang8-dbg
%define platformFixed x86_64_centos7_clang8_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_97a_LHCB_2LHCbGenerators_x86_64-centos7-clang8-dbg
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_97a_LHCB_2_alpgen_2.1.4_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_herwig++_2.7.1_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_hijing_1.383bs.2_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_lhapdf_6.2.3_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_photos++_3.56.lhcb1_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_pythia6_427.2.lhcb_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_pythia8_240.lhcb4_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_rivet_2.7.2b_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_tauola++_1.1.6b.lhcb_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_thepeg_1.9.2p1_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_crmc_1.5.6_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_starlight_r300_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_powheg-box-v2_r3043.lhcb.rdynamic_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_rivet_2.7.2b_x86_64_centos7_clang8_dbg
Requires: LCG_97a_LHCB_2_madgraph5amc_2.7.2.atlas_x86_64_centos7_clang8_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
