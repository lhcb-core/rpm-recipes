
%define version 97apython3
%define platform x86_64-centos7-clang8-opt
%define platformFixed x86_64_centos7_clang8_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_97apython3LHCb_x86_64-centos7-clang8-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_97apython3_AIDA_3.2.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_Boost_1.72.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_catboost_0.9.1.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_chardet_3.0.4_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_clhep_2.4.1.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_cppgsl_3.0.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_CppUnit_1.14.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_doxygen_1.8.15_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_eigen_3.3.7_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_fastjet_3.3.2_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_fftw_3.3.8_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_flatbuffers_1.11.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_gdb_9.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_graphviz_py_0.11.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_GSL_2.6_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_HepMC_2.06.11_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_HepPDT_2.06.01_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_idna_2.8_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_ipython_genutils_0.2.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_blas_0.3.9.openblas_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_libgit2_0.28.2_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_libunwind_1.3.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_libxml2_2.9.9_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_lxml_4.3.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_matplotlib_3.1.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_mpmath_1.1.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_networkx_2.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_oracle_19.3.0.0.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_pathos_0.2.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_Python_3.7.6_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_rangev3_0.10.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_RELAX_root6_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_ROOT_v6.20.06_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_sqlite_3280000_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_tbb_2020_U1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_gperftools_2.7_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_tensorflow_1.14.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_Vc_1.4.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_vdt_0.4.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_veccore_0.6.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_vectorclass_2.01.02_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_wcwidth_0.1.7_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_XercesC_3.1.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_xgboost_0.90_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_xqilla_2.3.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_xrootd_4.12.2_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_packaging_19.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_fmt_6.1.2_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_six_1.12.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_coverage_4.5.3_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_jemalloc_5.2.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_future_0.17.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_jsonmcpp_3.6.1_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_spdlog_1.5.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_gperftools_2.7_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_sortedcontainers_2.1.0_x86_64_centos7_clang8_opt
Requires: LCG_97apython3_yamlcpp_0.6.3_x86_64_centos7_clang8_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
