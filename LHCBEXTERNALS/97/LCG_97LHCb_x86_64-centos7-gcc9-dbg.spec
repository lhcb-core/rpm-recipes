
%define version 97
%define platform x86_64-centos7-gcc9-dbg
%define platformFixed x86_64_centos7_gcc9_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_97LHCb_x86_64-centos7-gcc9-dbg
Version: 1.0.0
Release: 3
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_97_AIDA_3.2.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_Boost_1.72.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_catboost_0.9.1.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_chardet_3.0.4_x86_64_centos7_gcc9_dbg
Requires: LCG_97_cppgsl_2.1.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_CppUnit_1.14.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_doxygen_1.8.15_x86_64_centos7_gcc9_dbg
Requires: LCG_97_eigen_3.3.7_x86_64_centos7_gcc9_dbg
Requires: LCG_97_fastjet_3.3.2_x86_64_centos7_gcc9_dbg
Requires: LCG_97_fftw_3.3.8_x86_64_centos7_gcc9_dbg
Requires: LCG_97_flatbuffers_1.11.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_gdb_8.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_graphviz_2.40.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_GSL_2.5_x86_64_centos7_gcc9_dbg
Requires: LCG_97_HepMC_2.06.10_x86_64_centos7_gcc9_dbg
Requires: LCG_97_HepPDT_2.06.01_x86_64_centos7_gcc9_dbg
Requires: LCG_97_idna_2.8_x86_64_centos7_gcc9_dbg
Requires: LCG_97_ipython_genutils_0.2.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_ipython_5.8.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_lapack_3.8.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_libgit2_0.28.2_x86_64_centos7_gcc9_dbg
Requires: LCG_97_libunwind_1.3.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_libxml2_2.9.9_x86_64_centos7_gcc9_dbg
Requires: LCG_97_lxml_4.3.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_matplotlib_2.2.4_x86_64_centos7_gcc9_dbg
Requires: LCG_97_mpmath_1.1.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_networkx_2.2_x86_64_centos7_gcc9_dbg
Requires: LCG_97_oracle_19.3.0.0.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_pathos_0.2.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_pyanalysis_2.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_pygraphics_2.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_pyqt5_5.12.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_Python_2.7.16_x86_64_centos7_gcc9_dbg
Requires: LCG_97_pytools_2019.1.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_QMtest_2.4.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_Qt5_5.12.4_x86_64_centos7_gcc9_dbg
Requires: LCG_97_rangev3_0.10.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_RELAX_root6_x86_64_centos7_gcc9_dbg
Requires: LCG_97_ROOT_v6.20.02_x86_64_centos7_gcc9_dbg
Requires: LCG_97_sqlite_3280000_x86_64_centos7_gcc9_dbg
Requires: LCG_97_tbb_2020_U1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_tensorflow_1.14.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_tensorflow_estimator_1.14.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_Vc_1.4.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_vdt_0.4.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_veccore_0.6.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_vectorclass_1.30p1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_wcwidth_0.1.7_x86_64_centos7_gcc9_dbg
Requires: LCG_97_XercesC_3.1.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_xgboost_0.90_x86_64_centos7_gcc9_dbg
Requires: LCG_97_xqilla_2.3.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_xrootd_4.11.2_x86_64_centos7_gcc9_dbg
Requires: LCG_97_packaging_19.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_fmt_6.1.2_x86_64_centos7_gcc9_dbg
Requires: LCG_97_six_1.12.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_coverage_4.5.3_x86_64_centos7_gcc9_dbg
Requires: LCG_97_jemalloc_5.2.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_future_0.17.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_jsonmcpp_3.6.1_x86_64_centos7_gcc9_dbg
Requires: LCG_97_spdlog_1.5.0_x86_64_centos7_gcc9_dbg
Requires: LCG_97_gperftools_2.7_x86_64_centos7_gcc9_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
