Externals used by LHCb
======================

Description
-----------
This directory replace the LHCBEXTERNALS cmt project that used to exist to create the LCG tar files.

Creating the LHCb meta RPMs for LCG externals
---------------------------------------------

Create the JSON file thats lists all the externals needed for a particular LCG version, see for example LHCBEXTERNALS_95.json for the syntax.
A file listing the platforms for the LCG version is also needed (c.f. LCG_95_platforms.txt)
To create all the metarpms for LCG 97a_LHCB_4:
```
export LCGVER=97a_LHCB_4
lb-create-all-lcg-meta-specs 'LCG_{lcg_version}LHCb_{platform}' LCG_${LCGVER}_platforms.txt LHCBEXTERNALS_${LCGVER}.json 

```

To build the RPMs containing directly the links to the the packages with checksum in their name:

```
export LCGVER=97a_LHCB_4
lb-create-all-lcg-meta-specs --require-base-package 'LCG_{lcg_version}LHCb_{platform}' LCG_${LCGVER}_platforms.txt LHCBEXTERNALS_${LCGVER}.json 
```



and follow the instructions to build the RPMs at the end of the script.

BEWARE: The repoinfo.json file in the repository may have to be updated as a new repository is created for new versions of LCG

Then to copy them to the LHCb yum repository:
```
 cp /tmp/rpmbuild/RPMS/noarch/*  /eos/project/l/lhcbwebsites/www/lhcb-rpm/lhcb2024/
 createrepo --update /eos/project/l/lhcbwebsites/www/lhcb-rpm/lhcb2024/
```

You need to update /eos/project/l/lhcbwebsites/www/lhcb-rpm/repoinfo.json by hand

The new LHCBEXTERNALS_${LCGVERSION}.json and LCG_${LCGVERSION}_platforms.txt have to be added to the rpm-recipes repo for further reference and update.



Creating the LHCb meta RPMs for LCG generators
----------------------------------------------
Same recipe as for externals, now that we have custom LCG releases



Installation on CVMFS
---------------------

Just login to the statum-0, install the meta RPMs produced in the previous step.

