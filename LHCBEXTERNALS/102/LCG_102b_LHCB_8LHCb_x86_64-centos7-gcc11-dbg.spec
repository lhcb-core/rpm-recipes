
%define version 102b_LHCB_8
%define platform x86_64-centos7-gcc11-dbg
%define platformFixed x86_64_centos7_gcc11_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_102b_LHCB_8LHCb_x86_64-centos7-gcc11-dbg
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_x86_64_centos7_gcc11_dbg
Requires: blas-6339b_0.3.20.openblas_x86_64_centos7_gcc11_dbg
Requires: Boost-e77cf_1.78.0_x86_64_centos7_gcc11_dbg
Requires: cachetools-08f48_3.1.1_x86_64_centos7_gcc11_dbg
Requires: catboost-cf88f_0.26.1_x86_64_centos7_gcc11_dbg
Requires: Catch2-7861b_2.13.9_x86_64_centos7_gcc11_dbg
Requires: chardet-85354_3.0.4_x86_64_centos7_gcc11_dbg
Requires: clhep-ebe73_2.4.5.1_x86_64_centos7_gcc11_dbg
Requires: click-07a71_8.0.3_x86_64_centos7_gcc11_dbg
Requires: coverage-cbd70_6.3.2_x86_64_centos7_gcc11_dbg
Requires: cppgsl-7a1b6_3.1.0_x86_64_centos7_gcc11_dbg
Requires: CppUnit-b79b3_1.14.0_x86_64_centos7_gcc11_dbg
Requires: crmc-49c90_1.8.0.lhcb_x86_64_centos7_gcc11_dbg
Requires: DD4hep-1dd89_01.23_x86_64_centos7_gcc11_dbg
Requires: doxygen-dffea_1.8.18_x86_64_centos7_gcc11_dbg
Requires: eigen-642a5_3.3.7_x86_64_centos7_gcc11_dbg
Requires: fastjet-af3e7_3.4.0p1_x86_64_centos7_gcc11_dbg
Requires: fftw-cd301_3.3.8_x86_64_centos7_gcc11_dbg
Requires: flatbuffers-24169_1.12.0_x86_64_centos7_gcc11_dbg
Requires: fmt-18428_7.1.3_x86_64_centos7_gcc11_dbg
Requires: future-cf5e0_0.17.1_x86_64_centos7_gcc11_dbg
Requires: gdb-78aaa_11.2_x86_64_centos7_gcc11_dbg
Requires: gperftools-cb5bd_2.9.1_x86_64_centos7_gcc11_dbg
Requires: graphviz-a682a_2.40.1_x86_64_centos7_gcc11_dbg
Requires: GitCondDB-9a857_0.2.1_x86_64_centos7_gcc11_dbg
Requires: GSL-30ba4_2.7_x86_64_centos7_gcc11_dbg
Requires: HepMC-d5a39_2.06.11_x86_64_centos7_gcc11_dbg
Requires: HepPDT-dba76_2.06.01_x86_64_centos7_gcc11_dbg
Requires: herwig3-cd61b_7.2.3p2_x86_64_centos7_gcc11_dbg
Requires: idna-e6055_3.2_x86_64_centos7_gcc11_dbg
Requires: ipython-5f199_7.25.0_x86_64_centos7_gcc11_dbg
Requires: jemalloc-8154a_5.2.1_x86_64_centos7_gcc11_dbg
Requires: Jinja2-766ba_3.0.1_x86_64_centos7_gcc11_dbg
Requires: jsonmcpp-f26c3_3.10.5_x86_64_centos7_gcc11_dbg
Requires: lhapdf-350c8_6.2.3p1_x86_64_centos7_gcc11_dbg
Requires: libgit2-98d83_1.1.1_x86_64_centos7_gcc11_dbg
Requires: libunwind-2fa0a_1.3.1_x86_64_centos7_gcc11_dbg
Requires: libxml2-9cd93_2.9.10_x86_64_centos7_gcc11_dbg
Requires: lxml-e7188_4.6.2_x86_64_centos7_gcc11_dbg
Requires: madgraph5amc-3e872_2.9.3.atlas1_x86_64_centos7_gcc11_dbg
Requires: matplotlib-3f48d_3.4.3_x86_64_centos7_gcc11_dbg
Requires: mpmath-115ae_1.2.1_x86_64_centos7_gcc11_dbg
Requires: networkx-814fe_2.5.1_x86_64_centos7_gcc11_dbg
Requires: oracle-69dfe_19.11.0.0.0_x86_64_centos7_gcc11_dbg
Requires: packaging-a60db_21.0_x86_64_centos7_gcc11_dbg
Requires: pathos-d6299_0.2.3_x86_64_centos7_gcc11_dbg
Requires: pydantic-a4a48_1.9.0_x86_64_centos7_gcc11_dbg
Requires: pydot-107bc_1.4.1_x86_64_centos7_gcc11_dbg
Requires: pytest-b9bb8_7.0.1_x86_64_centos7_gcc11_dbg
Requires: photos++-ef3aa_3.56.lhcb1_x86_64_centos7_gcc11_dbg
Requires: powheg-box-v2-dfdf8_r3744.lhcb4.rdynamic_x86_64_centos7_gcc11_dbg
Requires: pythia6-94243_427.2.lhcb_x86_64_centos7_gcc11_dbg
Requires: pythia8-83bde_244.lhcb4_x86_64_centos7_gcc11_dbg
Requires: Python-9a1bc_3.9.12_x86_64_centos7_gcc11_dbg
Requires: pyzmq-eb981_22.1.0_x86_64_centos7_gcc11_dbg
Requires: rangev3-79ab4_0.11.0_x86_64_centos7_gcc11_dbg
Requires: RELAX-e2c36_root6_x86_64_centos7_gcc11_dbg
Requires: rivet-9444d_3.1.10_x86_64_centos7_gcc11_dbg
Requires: rivet-9444d_3.1.10_x86_64_centos7_gcc11_dbg
Requires: ROOT-34ede_6.26.08_x86_64_centos7_gcc11_dbg
Requires: ruamel_yaml-b7164_0.17.16_x86_64_centos7_gcc11_dbg
Requires: six-7d0d9_1.16.0_x86_64_centos7_gcc11_dbg
Requires: sortedcontainers-f9052_2.1.0_x86_64_centos7_gcc11_dbg
Requires: spdlog-15a38_1.9.2_x86_64_centos7_gcc11_dbg
Requires: sqlite-3c47f_3320300_x86_64_centos7_gcc11_dbg
Requires: starlight-eaf9e_r313_x86_64_centos7_gcc11_dbg
Requires: sympy-96503_1.8_x86_64_centos7_gcc11_dbg
Requires: tauola++-4cffd_1.1.6b.lhcb_x86_64_centos7_gcc11_dbg
Requires: tbb-adcbe_2020_U2_x86_64_centos7_gcc11_dbg
Requires: tensorflow-a7d62_2.8.0_x86_64_centos7_gcc11_dbg
Requires: torch-7c1a4_1.11.0_x86_64_centos7_gcc11_dbg
Requires: thepeg-28d41_2.2.3_x86_64_centos7_gcc11_dbg
Requires: Vc-57098_1.4.3_x86_64_centos7_gcc11_dbg
Requires: vdt-7622c_0.4.3_x86_64_centos7_gcc11_dbg
Requires: veccore-b5c00_0.8.0_x86_64_centos7_gcc11_dbg
Requires: vectorclass-cdfdb_2.01.02_x86_64_centos7_gcc11_dbg
Requires: wcwidth-e6a86_0.2.5_x86_64_centos7_gcc11_dbg
Requires: wrapt-08b78_1.13.3_x86_64_centos7_gcc11_dbg
Requires: XercesC-714f6_3.2.3_x86_64_centos7_gcc11_dbg
Requires: xgboost-c54cb_0.90_x86_64_centos7_gcc11_dbg
Requires: xqilla-c5cd1_2.3.4_x86_64_centos7_gcc11_dbg
Requires: xrootd-1b6fc_5.4.3_x86_64_centos7_gcc11_dbg
Requires: yamlcpp-d05b2_0.6.3_x86_64_centos7_gcc11_dbg
Requires: yoda-084f9_1.9.10_x86_64_centos7_gcc11_dbg
Requires: PyYAML-edbb9_5.3.1_x86_64_centos7_gcc11_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
