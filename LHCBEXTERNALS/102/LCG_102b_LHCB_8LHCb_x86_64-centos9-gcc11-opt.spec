
%define version 102b_LHCB_8
%define platform x86_64-centos9-gcc11-opt
%define platformFixed x86_64_centos9_gcc11_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_102b_LHCB_8LHCb_x86_64-centos9-gcc11-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_x86_64_centos9_gcc11_opt
Requires: blas-6339b_0.3.20.openblas_x86_64_centos9_gcc11_opt
Requires: Boost-c319e_1.78.0_x86_64_centos9_gcc11_opt
Requires: cachetools-08f48_3.1.1_x86_64_centos9_gcc11_opt
Requires: catboost-c03a2_0.26.1_x86_64_centos9_gcc11_opt
Requires: Catch2-7861b_2.13.9_x86_64_centos9_gcc11_opt
Requires: chardet-85354_3.0.4_x86_64_centos9_gcc11_opt
Requires: clhep-ebe73_2.4.5.1_x86_64_centos9_gcc11_opt
Requires: click-07a71_8.0.3_x86_64_centos9_gcc11_opt
Requires: coverage-cbd70_6.3.2_x86_64_centos9_gcc11_opt
Requires: cppgsl-7a1b6_3.1.0_x86_64_centos9_gcc11_opt
Requires: CppUnit-b79b3_1.14.0_x86_64_centos9_gcc11_opt
Requires: crmc-0395c_1.8.0.lhcb_x86_64_centos9_gcc11_opt
Requires: DD4hep-3381d_01.23_x86_64_centos9_gcc11_opt
Requires: doxygen-18363_1.8.18_x86_64_centos9_gcc11_opt
Requires: eigen-642a5_3.3.7_x86_64_centos9_gcc11_opt
Requires: fastjet-af3e7_3.4.0p1_x86_64_centos9_gcc11_opt
Requires: fftw-cd301_3.3.8_x86_64_centos9_gcc11_opt
Requires: flatbuffers-24169_1.12.0_x86_64_centos9_gcc11_opt
Requires: fmt-18428_7.1.3_x86_64_centos9_gcc11_opt
Requires: future-cf5e0_0.17.1_x86_64_centos9_gcc11_opt
Requires: gdb-78aaa_11.2_x86_64_centos9_gcc11_opt
Requires: gperftools-36e73_2.9.1_x86_64_centos9_gcc11_opt
Requires: graphviz-cafb6_2.40.1_x86_64_centos9_gcc11_opt
Requires: GitCondDB-9a857_0.2.1_x86_64_centos9_gcc11_opt
Requires: GSL-30ba4_2.7_x86_64_centos9_gcc11_opt
Requires: HepMC-d5a39_2.06.11_x86_64_centos9_gcc11_opt
Requires: HepPDT-dba76_2.06.01_x86_64_centos9_gcc11_opt
Requires: herwig3-433a2_7.2.3p2_x86_64_centos9_gcc11_opt
Requires: idna-e6055_3.2_x86_64_centos9_gcc11_opt
Requires: ipython-5f199_7.25.0_x86_64_centos9_gcc11_opt
Requires: jemalloc-8154a_5.2.1_x86_64_centos9_gcc11_opt
Requires: Jinja2-766ba_3.0.1_x86_64_centos9_gcc11_opt
Requires: jsonmcpp-f26c3_3.10.5_x86_64_centos9_gcc11_opt
Requires: lhapdf-350c8_6.2.3p1_x86_64_centos9_gcc11_opt
Requires: libgit2-98d83_1.1.1_x86_64_centos9_gcc11_opt
Requires: libunwind-2fa0a_1.3.1_x86_64_centos9_gcc11_opt
Requires: lxml-1c52d_4.6.2_x86_64_centos9_gcc11_opt
Requires: madgraph5amc-3e872_2.9.3.atlas1_x86_64_centos9_gcc11_opt
Requires: matplotlib-01130_3.4.3_x86_64_centos9_gcc11_opt
Requires: mpmath-115ae_1.2.1_x86_64_centos9_gcc11_opt
Requires: networkx-814fe_2.5.1_x86_64_centos9_gcc11_opt
Requires: oracle-40bff_19.11.0.0.0_x86_64_centos9_gcc11_opt
Requires: packaging-a60db_21.0_x86_64_centos9_gcc11_opt
Requires: pathos-d6299_0.2.3_x86_64_centos9_gcc11_opt
Requires: pydantic-a4a48_1.9.0_x86_64_centos9_gcc11_opt
Requires: pydot-107bc_1.4.1_x86_64_centos9_gcc11_opt
Requires: pytest-b9bb8_7.0.1_x86_64_centos9_gcc11_opt
Requires: photos++-00260_3.56.lhcb1_x86_64_centos9_gcc11_opt
Requires: powheg-box-v2-dfdf8_r3744.lhcb4.rdynamic_x86_64_centos9_gcc11_opt
Requires: pythia6-94243_427.2.lhcb_x86_64_centos9_gcc11_opt
Requires: pythia8-abb2a_244.lhcb4_x86_64_centos9_gcc11_opt
Requires: Python-9a1bc_3.9.12_x86_64_centos9_gcc11_opt
Requires: pyzmq-eb981_22.1.0_x86_64_centos9_gcc11_opt
Requires: rangev3-79ab4_0.11.0_x86_64_centos9_gcc11_opt
Requires: RELAX-df984_root6_x86_64_centos9_gcc11_opt
Requires: rivet-89155_3.1.10_x86_64_centos9_gcc11_opt
Requires: rivet-89155_3.1.10_x86_64_centos9_gcc11_opt
Requires: ROOT-69213_6.26.08_x86_64_centos9_gcc11_opt
Requires: ruamel_yaml-b7164_0.17.16_x86_64_centos9_gcc11_opt
Requires: six-7d0d9_1.16.0_x86_64_centos9_gcc11_opt
Requires: sortedcontainers-f9052_2.1.0_x86_64_centos9_gcc11_opt
Requires: spdlog-15a38_1.9.2_x86_64_centos9_gcc11_opt
Requires: sqlite-3c47f_3320300_x86_64_centos9_gcc11_opt
Requires: starlight-eaf9e_r313_x86_64_centos9_gcc11_opt
Requires: sympy-96503_1.8_x86_64_centos9_gcc11_opt
Requires: tauola++-62ac6_1.1.6b.lhcb_x86_64_centos9_gcc11_opt
Requires: tbb-adcbe_2020_U2_x86_64_centos9_gcc11_opt
Requires: tensorflow-c4922_2.8.0_x86_64_centos9_gcc11_opt
Requires: torch-7c1a4_1.11.0_x86_64_centos9_gcc11_opt
Requires: thepeg-35813_2.2.3_x86_64_centos9_gcc11_opt
Requires: Vc-57098_1.4.3_x86_64_centos9_gcc11_opt
Requires: vdt-7622c_0.4.3_x86_64_centos9_gcc11_opt
Requires: veccore-b5c00_0.8.0_x86_64_centos9_gcc11_opt
Requires: vectorclass-cdfdb_2.01.02_x86_64_centos9_gcc11_opt
Requires: wcwidth-e6a86_0.2.5_x86_64_centos9_gcc11_opt
Requires: wrapt-08b78_1.13.3_x86_64_centos9_gcc11_opt
Requires: XercesC-714f6_3.2.3_x86_64_centos9_gcc11_opt
Requires: xgboost-c54cb_0.90_x86_64_centos9_gcc11_opt
Requires: xqilla-c5cd1_2.3.4_x86_64_centos9_gcc11_opt
Requires: xrootd-558a9_5.4.3_x86_64_centos9_gcc11_opt
Requires: yamlcpp-d05b2_0.6.3_x86_64_centos9_gcc11_opt
Requires: yoda-b334e_1.9.10_x86_64_centos9_gcc11_opt
Requires: PyYAML-edbb9_5.3.1_x86_64_centos9_gcc11_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
