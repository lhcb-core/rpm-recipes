
%define version 102b_LHCB_8
%define platform aarch64-centos7-gcc11-opt
%define platformFixed aarch64_centos7_gcc11_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_102b_LHCB_8LHCb_aarch64-centos7-gcc11-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_aarch64_centos7_gcc11_opt
Requires: blas-e5f0c_0.3.17.openblas_aarch64_centos7_gcc11_opt
Requires: Boost-479ff_1.78.0_aarch64_centos7_gcc11_opt
Requires: cachetools-08f48_3.1.1_aarch64_centos7_gcc11_opt
Requires: Catch2-7861b_2.13.9_aarch64_centos7_gcc11_opt
Requires: chardet-85354_3.0.4_aarch64_centos7_gcc11_opt
Requires: clhep-ebe73_2.4.5.1_aarch64_centos7_gcc11_opt
Requires: click-07a71_8.0.3_aarch64_centos7_gcc11_opt
Requires: coverage-cbd70_6.3.2_aarch64_centos7_gcc11_opt
Requires: cppgsl-7a1b6_3.1.0_aarch64_centos7_gcc11_opt
Requires: CppUnit-b79b3_1.14.0_aarch64_centos7_gcc11_opt
Requires: crmc-5599a_1.8.0.lhcb_aarch64_centos7_gcc11_opt
Requires: DD4hep-4691e_01.23_aarch64_centos7_gcc11_opt
Requires: doxygen-dffea_1.8.18_aarch64_centos7_gcc11_opt
Requires: eigen-642a5_3.3.7_aarch64_centos7_gcc11_opt
Requires: fastjet-af3e7_3.4.0p1_aarch64_centos7_gcc11_opt
Requires: fftw-cd301_3.3.8_aarch64_centos7_gcc11_opt
Requires: flatbuffers-ddc78_1.12.0_aarch64_centos7_gcc11_opt
Requires: fmt-18428_7.1.3_aarch64_centos7_gcc11_opt
Requires: future-cf5e0_0.17.1_aarch64_centos7_gcc11_opt
Requires: gdb-78aaa_11.2_aarch64_centos7_gcc11_opt
Requires: gperftools-cb5bd_2.9.1_aarch64_centos7_gcc11_opt
Requires: graphviz-a682a_2.40.1_aarch64_centos7_gcc11_opt
Requires: GitCondDB-9a857_0.2.1_aarch64_centos7_gcc11_opt
Requires: GSL-30ba4_2.7_aarch64_centos7_gcc11_opt
Requires: HepMC-d5a39_2.06.11_aarch64_centos7_gcc11_opt
Requires: HepPDT-dba76_2.06.01_aarch64_centos7_gcc11_opt
Requires: herwig3-604a0_7.2.3p2_aarch64_centos7_gcc11_opt
Requires: idna-e6055_3.2_aarch64_centos7_gcc11_opt
Requires: ipython-5f199_7.25.0_aarch64_centos7_gcc11_opt
Requires: jemalloc-8154a_5.2.1_aarch64_centos7_gcc11_opt
Requires: Jinja2-766ba_3.0.1_aarch64_centos7_gcc11_opt
Requires: jsonmcpp-f26c3_3.10.5_aarch64_centos7_gcc11_opt
Requires: lhapdf-350c8_6.2.3p1_aarch64_centos7_gcc11_opt
Requires: libgit2-98d83_1.1.1_aarch64_centos7_gcc11_opt
Requires: libunwind-2fa0a_1.3.1_aarch64_centos7_gcc11_opt
Requires: libxml2-9cd93_2.9.10_aarch64_centos7_gcc11_opt
Requires: lxml-e7188_4.6.2_aarch64_centos7_gcc11_opt
Requires: madgraph5amc-3e872_2.9.3.atlas1_aarch64_centos7_gcc11_opt
Requires: matplotlib-92372_3.4.3_aarch64_centos7_gcc11_opt
Requires: mpmath-115ae_1.2.1_aarch64_centos7_gcc11_opt
Requires: networkx-814fe_2.5.1_aarch64_centos7_gcc11_opt
Requires: oracle-ea30e_19.10.0.0.0_aarch64_centos7_gcc11_opt
Requires: packaging-a60db_21.0_aarch64_centos7_gcc11_opt
Requires: pathos-d6299_0.2.3_aarch64_centos7_gcc11_opt
Requires: pydantic-a4a48_1.9.0_aarch64_centos7_gcc11_opt
Requires: pydot-107bc_1.4.1_aarch64_centos7_gcc11_opt
Requires: pytest-b9bb8_7.0.1_aarch64_centos7_gcc11_opt
Requires: photos++-d92b6_3.56.lhcb1_aarch64_centos7_gcc11_opt
Requires: powheg-box-v2-dfdf8_r3744.lhcb4.rdynamic_aarch64_centos7_gcc11_opt
Requires: pythia6-94243_427.2.lhcb_aarch64_centos7_gcc11_opt
Requires: pythia8-83bde_244.lhcb4_aarch64_centos7_gcc11_opt
Requires: Python-9a1bc_3.9.12_aarch64_centos7_gcc11_opt
Requires: pyzmq-eb981_22.1.0_aarch64_centos7_gcc11_opt
Requires: rangev3-79ab4_0.11.0_aarch64_centos7_gcc11_opt
Requires: RELAX-11a97_root6_aarch64_centos7_gcc11_opt
Requires: rivet-f09b9_3.1.10_aarch64_centos7_gcc11_opt
Requires: rivet-f09b9_3.1.10_aarch64_centos7_gcc11_opt
Requires: ROOT-ec51b_6.26.08_aarch64_centos7_gcc11_opt
Requires: ruamel_yaml-b7164_0.17.16_aarch64_centos7_gcc11_opt
Requires: six-7d0d9_1.16.0_aarch64_centos7_gcc11_opt
Requires: sortedcontainers-f9052_2.1.0_aarch64_centos7_gcc11_opt
Requires: spdlog-15a38_1.9.2_aarch64_centos7_gcc11_opt
Requires: sqlite-3c47f_3320300_aarch64_centos7_gcc11_opt
Requires: starlight-eaf9e_r313_aarch64_centos7_gcc11_opt
Requires: sympy-96503_1.8_aarch64_centos7_gcc11_opt
Requires: tauola++-4de67_1.1.6b.lhcb_aarch64_centos7_gcc11_opt
Requires: tbb-adcbe_2020_U2_aarch64_centos7_gcc11_opt
Requires: thepeg-4148b_2.2.3_aarch64_centos7_gcc11_opt
Requires: Vc-57098_1.4.3_aarch64_centos7_gcc11_opt
Requires: vdt-7622c_0.4.3_aarch64_centos7_gcc11_opt
Requires: veccore-b5c00_0.8.0_aarch64_centos7_gcc11_opt
Requires: vectorclass-cdfdb_2.01.02_aarch64_centos7_gcc11_opt
Requires: wcwidth-e6a86_0.2.5_aarch64_centos7_gcc11_opt
Requires: wrapt-08b78_1.13.3_aarch64_centos7_gcc11_opt
Requires: XercesC-714f6_3.2.3_aarch64_centos7_gcc11_opt
Requires: xgboost-2df90_0.90_aarch64_centos7_gcc11_opt
Requires: xrootd-1b6fc_5.4.3_aarch64_centos7_gcc11_opt
Requires: yamlcpp-d05b2_0.6.3_aarch64_centos7_gcc11_opt
Requires: yoda-f5bba_1.9.10_aarch64_centos7_gcc11_opt
Requires: PyYAML-edbb9_5.3.1_aarch64_centos7_gcc11_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
