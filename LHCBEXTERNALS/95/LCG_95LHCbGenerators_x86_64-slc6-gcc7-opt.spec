
%define version 95
%define platform x86_64-slc6-gcc7-opt
%define platformFixed x86_64_slc6_gcc7_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_95LHCbGenerators_x86_64-slc6-gcc7-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_95_alpgen_2.1.4_x86_64_slc6_gcc7_opt
Requires: LCG_95_herwig++_2.7.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_herwig3_7.1.4_x86_64_slc6_gcc7_opt
Requires: LCG_95_hijing_1.383bs.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_lhapdf_6.1.6.cxxstd_x86_64_slc6_gcc7_opt
Requires: LCG_95_photos++_3.56_x86_64_slc6_gcc7_opt
Requires: LCG_95_powheg-box-v2_r3043.lhcb_x86_64_slc6_gcc7_opt
Requires: LCG_95_powheg-box-v2_r3043.lhcb.rdynamic_x86_64_slc6_gcc7_opt
Requires: LCG_95_pythia6_427.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_pythia8_235_x86_64_slc6_gcc7_opt
Requires: LCG_95_rivet_2.6.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_tauola++_1.1.6b.lhcb_x86_64_slc6_gcc7_opt
Requires: LCG_95_thepeg_2.1.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_crmc_1.5.6_x86_64_slc6_gcc7_opt
Requires: LCG_95_yoda_1.6.7_x86_64_slc6_gcc7_opt
Requires: LCG_95_starlight_r300_x86_64_slc6_gcc7_opt
Requires: LCG_95_madgraph5amc_2.6.1.atlas_x86_64_slc6_gcc7_opt
Requires: LCG_95_sherpa_2.2.5_x86_64_slc6_gcc7_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
