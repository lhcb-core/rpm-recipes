
%define version 95
%define platform x86_64-slc6-gcc7-opt
%define platformFixed x86_64_slc6_gcc7_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_%{version}LHCb_%{platformFixed}
Version: 1.0.0
Release: 3
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_95_libgit2_0.27.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_sqlite_3210000_x86_64_slc6_gcc7_opt
Requires: LCG_95_xgboost_0.71_x86_64_slc6_gcc7_opt
Requires: LCG_95_pyqt5_5.11.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_eigen_3.3.7_x86_64_slc6_gcc7_opt
Requires: LCG_95_Frontier_Client_2.8.19_x86_64_slc6_gcc7_opt
Requires: LCG_95_libxml2_2.9.7_x86_64_slc6_gcc7_opt
Requires: LCG_95_AIDA_3.2.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_rangev3_0.4.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_HepMC_2.06.09_x86_64_slc6_gcc7_opt
Requires: LCG_95_graphviz_2.28.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_gdb_8.2.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_pyanalysis_2.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_networkx_2.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_Boost_1.69.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_GSL_2.5_x86_64_slc6_gcc7_opt
Requires: LCG_95_pygraphics_2.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_lxml_4.2.3_x86_64_slc6_gcc7_opt
Requires: LCG_95_CLHEP_2.4.1.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_xqilla_2.3.3_x86_64_slc6_gcc7_opt
Requires: LCG_95_lapack_3.5.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_tbb_2019_U1_x86_64_slc6_gcc7_opt
Requires: LCG_95_pytools_2.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_HepPDT_2.06.01_x86_64_slc6_gcc7_opt
Requires: LCG_95_fastjet_3.3.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_ROOT_6.16.00_x86_64_slc6_gcc7_opt
Requires: LCG_95_veccore_0.4.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_vectorclass_1.30_x86_64_slc6_gcc7_opt
Requires: LCG_95_XercesC_3.1.3_x86_64_slc6_gcc7_opt
Requires: LCG_95_Qt5_5.11.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_CORAL_3_2_0_x86_64_slc6_gcc7_opt
Requires: LCG_95_wcwidth_0.1.7_x86_64_slc6_gcc7_opt
Requires: LCG_95_flatbuffers_1.8.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_matplotlib_2.2.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_Vc_1.3.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_xrootd_python_0.3.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_libunwind_5c2cade_x86_64_slc6_gcc7_opt
Requires: LCG_95_DD4hep_01_10_x86_64_slc6_gcc7_opt
Requires: LCG_95_ipython_genutils_0.2.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_neurobayes_expert_3.7.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_pathos_0.2.2.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_CppUnit_1.12.1_p1_x86_64_slc6_gcc7_opt
Requires: LCG_95_COOL_3_2_0_x86_64_slc6_gcc7_opt
Requires: LCG_95_doxygen_1.8.11_x86_64_slc6_gcc7_opt
Requires: LCG_95_vdt_0.4.2_x86_64_slc6_gcc7_opt
Requires: LCG_95_RELAX_root6_x86_64_slc6_gcc7_opt
Requires: LCG_95_QMtest_2.4.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_fftw_3.3.4_x86_64_slc6_gcc7_opt
Requires: LCG_95_catboost_0.9.1.1_x86_64_slc6_gcc7_opt
Requires: LCG_95_Python_2.7.15_x86_64_slc6_gcc7_opt
Requires: LCG_95_mpmath_1.0.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_chardet_3.0.4_x86_64_slc6_gcc7_opt
Requires: LCG_95_oracle_11.2.0.3.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_cppgsl_2.0.0_x86_64_slc6_gcc7_opt
Requires: LCG_95_idna_2.7_x86_64_slc6_gcc7_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
