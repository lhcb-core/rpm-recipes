
%define version 105a_LHCB_7
%define platform aarch64-el9-gcc13-dbg
%define platformFixed aarch64_el9_gcc13_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_105a_LHCB_7LHCb_aarch64-el9-gcc13-dbg
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_aarch64_el9_gcc13_dbg
Requires: Boost-9c64a_1.82.0_aarch64_el9_gcc13_dbg
Requires: Catch2-31748_2.13.9_aarch64_el9_gcc13_dbg
Requires: CppUnit-b79b3_1.14.0_aarch64_el9_gcc13_dbg
Requires: DD4hep-a7545_01.27.02_aarch64_el9_gcc13_dbg
Requires: GSL-30ba4_2.7_aarch64_el9_gcc13_dbg
Requires: GitCondDB-40070_0.2.2_aarch64_el9_gcc13_dbg
Requires: HepMC-d5a39_2.06.11_aarch64_el9_gcc13_dbg
Requires: HepPDT-dba76_2.06.01_aarch64_el9_gcc13_dbg
Requires: Jinja2-7c1ef_3.1.2_aarch64_el9_gcc13_dbg
Requires: PyYAML-45754_6.0.1_aarch64_el9_gcc13_dbg
Requires: Python-9a1bc_3.9.12_aarch64_el9_gcc13_dbg
Requires: RELAX-06963_6.1.1_aarch64_el9_gcc13_dbg
Requires: ROOT-53599_6.30.04_aarch64_el9_gcc13_dbg
Requires: Vc-9e9f2_1.4.4_aarch64_el9_gcc13_dbg
Requires: XercesC-9e637_3.2.4_aarch64_el9_gcc13_dbg
Requires: blas-e5f0c_0.3.17.openblas_aarch64_el9_gcc13_dbg
Requires: cachetools-c5642_5.3.1_aarch64_el9_gcc13_dbg
Requires: catboost-d6adc_1.2_aarch64_el9_gcc13_dbg
Requires: chardet-85354_3.0.4_aarch64_el9_gcc13_dbg
Requires: clhep-b7a7d_2.4.7.1_aarch64_el9_gcc13_dbg
Requires: click-227a4_8.1.7_aarch64_el9_gcc13_dbg
Requires: coverage-cbd70_6.3.2_aarch64_el9_gcc13_dbg
Requires: cppgsl-7a1b6_3.1.0_aarch64_el9_gcc13_dbg
Requires: crmc-b401d_1.8.0.lhcb_aarch64_el9_gcc13_dbg
Requires: doxygen-93ccb_1.8.18_aarch64_el9_gcc13_dbg
Requires: eigen-6ce89_3.4.0_aarch64_el9_gcc13_dbg
Requires: fastjet-5af57_3.4.1_aarch64_el9_gcc13_dbg
Requires: fftw-33229_3.3.10_aarch64_el9_gcc13_dbg
Requires: flatbuffers-be72e_23.5.26_aarch64_el9_gcc13_dbg
Requires: fmt-f6609_10.0.0_aarch64_el9_gcc13_dbg
Requires: future-4d201_0.18.3_aarch64_el9_gcc13_dbg
Requires: gdb-32249_14.1_aarch64_el9_gcc13_dbg
Requires: gperftools-16d5d_2.9.1_aarch64_el9_gcc13_dbg
Requires: graphviz-44ca1_8.0.5_aarch64_el9_gcc13_dbg
Requires: herwig3-5a2c3_7.2.1_aarch64_el9_gcc13_dbg
Requires: idna-e6055_3.2_aarch64_el9_gcc13_dbg
Requires: ipython-e0fd9_7.25.0_aarch64_el9_gcc13_dbg
Requires: jemalloc-5a247_5.3.0_aarch64_el9_gcc13_dbg
Requires: jsonmcpp-f26c3_3.10.5_aarch64_el9_gcc13_dbg
Requires: lhapdf-79828_6.2.3_aarch64_el9_gcc13_dbg
Requires: libgit2-98d83_1.1.1_aarch64_el9_gcc13_dbg
Requires: libunwind-2fa0a_1.3.1_aarch64_el9_gcc13_dbg
Requires: libxml2-87838_2.10.4_aarch64_el9_gcc13_dbg
Requires: lxml-42573_4.6.2_aarch64_el9_gcc13_dbg
Requires: madgraph5amc-32303_2.9.3.atlas1_aarch64_el9_gcc13_dbg
Requires: matplotlib-16065_3.8.0_aarch64_el9_gcc13_dbg
Requires: mpmath-b5202_1.2.1_aarch64_el9_gcc13_dbg
Requires: networkx-037ea_2.8.4_aarch64_el9_gcc13_dbg
Requires: oracle-da6b7_19.10.0.0.0_aarch64_el9_gcc13_dbg
Requires: packaging-24842_23.1_aarch64_el9_gcc13_dbg
Requires: pandas-4f368_1.5.3_aarch64_el9_gcc13_dbg
Requires: pathos-ab978_0.2.3_aarch64_el9_gcc13_dbg
Requires: photos++-29b42_3.56.lhcb1_aarch64_el9_gcc13_dbg
Requires: powheg-box-v2-0f8a9_r3744.lhcb4.rdynamic_aarch64_el9_gcc13_dbg
Requires: pydantic-2aa2a_1.10.9_aarch64_el9_gcc13_dbg
Requires: pydot-107bc_1.4.1_aarch64_el9_gcc13_dbg
Requires: pytest-1f66f_7.4.0_aarch64_el9_gcc13_dbg
Requires: pytest_cov-52f5d_3.0.0_aarch64_el9_gcc13_dbg
Requires: pythia6-94243_427.2.lhcb_aarch64_el9_gcc13_dbg
Requires: pythia8-81c49_244.lhcb4_aarch64_el9_gcc13_dbg
Requires: pyzmq-3ac85_22.1.0_aarch64_el9_gcc13_dbg
Requires: rangev3-79ab4_0.11.0_aarch64_el9_gcc13_dbg
Requires: rivet-14079_3.1.4_aarch64_el9_gcc13_dbg
Requires: ruamel_yaml-0b8d1_0.17.31_aarch64_el9_gcc13_dbg
Requires: six-7d0d9_1.16.0_aarch64_el9_gcc13_dbg
Requires: sortedcontainers-f9052_2.1.0_aarch64_el9_gcc13_dbg
Requires: spdlog-15a38_1.9.2_aarch64_el9_gcc13_dbg
Requires: sqlite-3c47f_3320300_aarch64_el9_gcc13_dbg
Requires: starlight-62bcd_r313_aarch64_el9_gcc13_dbg
Requires: sympy-42760_1.8_aarch64_el9_gcc13_dbg
Requires: tauola++-d23c6_1.1.6b.lhcb_aarch64_el9_gcc13_dbg
Requires: tbb-2a247_2021.10.0_aarch64_el9_gcc13_dbg
Requires: thepeg-84265_2.2.1_aarch64_el9_gcc13_dbg
Requires: valgrind-113bc_3.22.0_aarch64_el9_gcc13_dbg
Requires: vdt-260e4_0.4.4_aarch64_el9_gcc13_dbg
Requires: veccore-53953_0.8.1_aarch64_el9_gcc13_dbg
Requires: vectorclass-cdfdb_2.01.02_aarch64_el9_gcc13_dbg
Requires: wcwidth-e6a86_0.2.5_aarch64_el9_gcc13_dbg
Requires: wrapt-82b73_1.14.1_aarch64_el9_gcc13_dbg
Requires: xgboost-4916e_0.90_aarch64_el9_gcc13_dbg
Requires: xrootd-583b6_5.6.3_aarch64_el9_gcc13_dbg
Requires: yamlcpp-d05b2_0.6.3_aarch64_el9_gcc13_dbg
Requires: yoda-bcf4f_1.9.0_aarch64_el9_gcc13_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
