
%define version 105c_LHCB_8
%define platform x86_64-el9-clang16-opt
%define platformFixed x86_64_el9_clang16_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_105c_LHCB_8LHCb_x86_64-el9-clang16-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: AIDA-3fe9f_3.2.1_x86_64_el9_clang16_opt
Requires: Boost-fbfc9_1.82.0_x86_64_el9_clang16_opt
Requires: Catch2-31748_2.13.9_x86_64_el9_clang16_opt
Requires: CppUnit-b79b3_1.14.0_x86_64_el9_clang16_opt
Requires: DD4hep-5ab16_01.29_x86_64_el9_clang16_opt
Requires: GSL-30ba4_2.7_x86_64_el9_clang16_opt
Requires: GitCondDB-40070_0.2.2_x86_64_el9_clang16_opt
Requires: HepMC-d5a39_2.06.11_x86_64_el9_clang16_opt
Requires: HepPDT-dba76_2.06.01_x86_64_el9_clang16_opt
Requires: Jinja2-7c1ef_3.1.2_x86_64_el9_clang16_opt
Requires: PyYAML-45754_6.0.1_x86_64_el9_clang16_opt
Requires: Python-9a1bc_3.9.12_x86_64_el9_clang16_opt
Requires: RELAX-8b497_6.1.1_x86_64_el9_clang16_opt
Requires: ROOT-9d8ab_6.30.08_x86_64_el9_clang16_opt
Requires: Vc-9e9f2_1.4.4_x86_64_el9_clang16_opt
Requires: XercesC-9e637_3.2.4_x86_64_el9_clang16_opt
Requires: blas-c07f1_0.3.20.openblas_x86_64_el9_clang16_opt
Requires: cachetools-c5642_5.3.1_x86_64_el9_clang16_opt
Requires: catboost-d6adc_1.2_x86_64_el9_clang16_opt
Requires: chardet-85354_3.0.4_x86_64_el9_clang16_opt
Requires: clhep-b7a7d_2.4.7.1_x86_64_el9_clang16_opt
Requires: click-227a4_8.1.7_x86_64_el9_clang16_opt
Requires: coverage-cbd70_6.3.2_x86_64_el9_clang16_opt
Requires: cppgsl-7a1b6_3.1.0_x86_64_el9_clang16_opt
Requires: crmc-9102c_1.8.0.lhcb_x86_64_el9_clang16_opt
Requires: doxygen-93ccb_1.8.18_x86_64_el9_clang16_opt
Requires: eigen-6ce89_3.4.0_x86_64_el9_clang16_opt
Requires: fastjet-5af57_3.4.1_x86_64_el9_clang16_opt
Requires: fftw-33229_3.3.10_x86_64_el9_clang16_opt
Requires: flatbuffers-85508_23.5.26_x86_64_el9_clang16_opt
Requires: fmt-f6609_10.0.0_x86_64_el9_clang16_opt
Requires: future-4d201_0.18.3_x86_64_el9_clang16_opt
Requires: gdb-32249_14.1_x86_64_el9_clang16_opt
Requires: gperftools-16d5d_2.9.1_x86_64_el9_clang16_opt
Requires: graphviz-44ca1_8.0.5_x86_64_el9_clang16_opt
Requires: herwig3-b4cbe_7.2.3p2_x86_64_el9_clang16_opt
Requires: idna-e6055_3.2_x86_64_el9_clang16_opt
Requires: ipython-e0fd9_7.25.0_x86_64_el9_clang16_opt
Requires: jemalloc-5a247_5.3.0_x86_64_el9_clang16_opt
Requires: jsonmcpp-f26c3_3.10.5_x86_64_el9_clang16_opt
Requires: lhapdf-1b5ec_6.2.3p1_x86_64_el9_clang16_opt
Requires: libgit2-98d83_1.1.1_x86_64_el9_clang16_opt
Requires: libunwind-2fa0a_1.3.1_x86_64_el9_clang16_opt
Requires: libxml2-87838_2.10.4_x86_64_el9_clang16_opt
Requires: lxml-42573_4.6.2_x86_64_el9_clang16_opt
Requires: madgraph5amc-dff1c_2.9.3.atlas1_x86_64_el9_clang16_opt
Requires: matplotlib-a6b55_3.8.0_x86_64_el9_clang16_opt
Requires: mpmath-b5202_1.2.1_x86_64_el9_clang16_opt
Requires: networkx-91278_2.8.4_x86_64_el9_clang16_opt
Requires: oracle-ea4bc_19.19.0.0.0_x86_64_el9_clang16_opt
Requires: packaging-24842_23.1_x86_64_el9_clang16_opt
Requires: pandas-10c20_1.5.3_x86_64_el9_clang16_opt
Requires: pathos-ab978_0.2.3_x86_64_el9_clang16_opt
Requires: photos++-29b42_3.56.lhcb1_x86_64_el9_clang16_opt
Requires: pydantic-2aa2a_1.10.9_x86_64_el9_clang16_opt
Requires: pydot-107bc_1.4.1_x86_64_el9_clang16_opt
Requires: pytest-1f66f_7.4.0_x86_64_el9_clang16_opt
Requires: pytest_cov-52f5d_3.0.0_x86_64_el9_clang16_opt
Requires: pythia6-94243_427.2.lhcb_x86_64_el9_clang16_opt
Requires: pythia8-ac2f6_244.lhcb4_x86_64_el9_clang16_opt
Requires: pyzmq-3ac85_22.1.0_x86_64_el9_clang16_opt
Requires: rangev3-79ab4_0.11.0_x86_64_el9_clang16_opt
Requires: rivet-d1ea5_3.1.10_x86_64_el9_clang16_opt
Requires: ruamel_yaml-0b8d1_0.17.31_x86_64_el9_clang16_opt
Requires: shapely-9ce8e_1.7.1_x86_64_el9_clang16_opt
Requires: six-7d0d9_1.16.0_x86_64_el9_clang16_opt
Requires: sortedcontainers-f9052_2.1.0_x86_64_el9_clang16_opt
Requires: spdlog-15a38_1.9.2_x86_64_el9_clang16_opt
Requires: sqlite-3c47f_3320300_x86_64_el9_clang16_opt
Requires: starlight-62bcd_r313_x86_64_el9_clang16_opt
Requires: sympy-42760_1.8_x86_64_el9_clang16_opt
Requires: tauola++-481fc_1.1.6b.lhcb_x86_64_el9_clang16_opt
Requires: tbb-2a247_2021.10.0_x86_64_el9_clang16_opt
Requires: tensorflow-9c577_2.13.1_x86_64_el9_clang16_opt
Requires: thepeg-8c18c_2.2.3_x86_64_el9_clang16_opt
Requires: torch-34c1e_2.1.1_x86_64_el9_clang16_opt
Requires: valgrind-113bc_3.22.0_x86_64_el9_clang16_opt
Requires: vdt-260e4_0.4.4_x86_64_el9_clang16_opt
Requires: veccore-53953_0.8.1_x86_64_el9_clang16_opt
Requires: vectorclass-cdfdb_2.01.02_x86_64_el9_clang16_opt
Requires: wcwidth-e6a86_0.2.5_x86_64_el9_clang16_opt
Requires: wrapt-82b73_1.14.1_x86_64_el9_clang16_opt
Requires: xgboost-f5275_0.90_x86_64_el9_clang16_opt
Requires: xqilla-f303e_2.3.4_x86_64_el9_clang16_opt
Requires: xrootd-f815b_5.6.3_x86_64_el9_clang16_opt
Requires: yamlcpp-d05b2_0.6.3_x86_64_el9_clang16_opt
Requires: yoda-c95bd_1.9.10_x86_64_el9_clang16_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
