
%define version 100
%define platform x86_64-centos7-gcc10-dbg
%define platformFixed x86_64_centos7_gcc10_dbg

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_100LHCb_x86_64-centos7-gcc10-dbg
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_100_AIDA_3.2.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_Boost_1.75.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_catboost_0.9.1.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_chardet_3.0.4_x86_64_centos7_gcc10_dbg
Requires: LCG_100_clhep_2.4.4.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_cppgsl_3.1.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_CppUnit_1.14.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_doxygen_1.8.18_x86_64_centos7_gcc10_dbg
Requires: LCG_100_eigen_3.3.7_x86_64_centos7_gcc10_dbg
Requires: LCG_100_fastjet_3.3.4_x86_64_centos7_gcc10_dbg
Requires: LCG_100_fftw_3.3.8_x86_64_centos7_gcc10_dbg
Requires: LCG_100_flatbuffers_1.12.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_gdb_10.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_graphviz_2.40.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_graphviz_py_0.11.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_GSL_2.6_x86_64_centos7_gcc10_dbg
Requires: LCG_100_HepMC_2.06.11_x86_64_centos7_gcc10_dbg
Requires: LCG_100_HepPDT_2.06.01_x86_64_centos7_gcc10_dbg
Requires: LCG_100_idna_2.8_x86_64_centos7_gcc10_dbg
Requires: LCG_100_ipython_7.5.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_ipython_genutils_0.2.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_blas_0.3.10.openblas_x86_64_centos7_gcc10_dbg
Requires: LCG_100_libgit2_1.0.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_libunwind_1.3.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_libxml2_2.9.10_x86_64_centos7_gcc10_dbg
Requires: LCG_100_lxml_4.6.2_x86_64_centos7_gcc10_dbg
Requires: LCG_100_matplotlib_3.3.4_x86_64_centos7_gcc10_dbg
Requires: LCG_100_mpmath_1.1.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_networkx_2.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_oracle_19.3.0.0.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_pathos_0.2.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_Python_3.8.6_x86_64_centos7_gcc10_dbg
Requires: LCG_100_rangev3_0.11.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_RELAX_root6_x86_64_centos7_gcc10_dbg
Requires: LCG_100_ROOT_v6.24.00_x86_64_centos7_gcc10_dbg
Requires: LCG_100_sqlite_3320300_x86_64_centos7_gcc10_dbg
Requires: LCG_100_tbb_2020_U2_x86_64_centos7_gcc10_dbg
Requires: LCG_100_gperftools_2.9.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_tensorflow_estimator_2.3.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_tensorflow_2.3.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_Vc_1.4.1p1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_vdt_0.4.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_veccore_0.6.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_vectorclass_2.01.02_x86_64_centos7_gcc10_dbg
Requires: LCG_100_wcwidth_0.1.7_x86_64_centos7_gcc10_dbg
Requires: LCG_100_XercesC_3.2.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_xgboost_0.90_x86_64_centos7_gcc10_dbg
Requires: LCG_100_xqilla_2.3.4_x86_64_centos7_gcc10_dbg
Requires: LCG_100_xrootd_5.1.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_packaging_19.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_fmt_7.1.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_six_1.12.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_coverage_4.5.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_jemalloc_5.2.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_future_0.17.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_jsonmcpp_3.9.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_spdlog_1.5.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_gperftools_2.9.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_sortedcontainers_2.1.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_yamlcpp_0.6.3_x86_64_centos7_gcc10_dbg
Requires: LCG_100_wrapt_1.11.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_pytest_runner_5.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_pytest_6.1.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_click_7.0_x86_64_centos7_gcc10_dbg
Requires: LCG_100_cachetools_3.1.1_x86_64_centos7_gcc10_dbg
Requires: LCG_100_torch_1.7.0_x86_64_centos7_gcc10_dbg

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
