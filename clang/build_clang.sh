#!/bin/bash

set -e -x

version=4.0.1

packs="llvm cfe compiler-rt libcxx libcxxabi clang-tools-extra"

export PATH=$(echo $PATH | tr : \\n | grep -v ccache | tr \\n :)
export PATH=${PATH%:}
export PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.7.2/Linux-x86_64/bin:$PATH

mkdir llvm
#mv .ccache llvm

cd llvm

for p in $packs ; do
  wget http://releases.llvm.org/${version}/${p}-${version}.src.tar.xz
  tar xf ${p}-${version}.src.tar.xz
done

mv llvm-${version}.src llvm
mv cfe-${version}.src llvm/tools/clang
mv clang-tools-extra-${version}.src llvm/tools/clang/tools/extra
mv compiler-rt-${version}.src llvm/projects/compiler-rt
mv libcxx-${version}.src llvm/projects/libcxx
mv libcxxabi-${version}.src llvm/projects/libcxxabi

mkdir build
cd build
cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/lcg/llvm/${version}/x86_64-centos7 \
  -DLLVM_CCACHE_BUILD=ON -DLLVM_CCACHE_DIR=$(dirname $PWD)/.ccache \
  -DCPACK_PACKAGING_INSTALL_PREFIX=/opt/lcg/llvm/${version}/x86_64-centos7 \
  -DCPACK_RPM_PACKAGE_RELOCATABLE=TRUE -DCPACK_RPM_RELOCATION_PATHS=/opt/lcg -DCPACK_RPM_NO_INSTALL_PREFIX_RELOCATION=ON \
  -DCPACK_PACKAGE_NAME=llvm_${version}_x86_64_centos7 -DCPACK_RPM_PACKAGE_ARCHITECTURE=noarch \
  -DCPACK_RPM_PACKAGE_AUTOREQPROV=NO \
  ../llvm

ninja
cpack -G RPM

mv llvm_${version}_x86_64_centos7-${version}-Linux.rpm ../../llvm_${version}_x86_64_centos7-${version}-1.noarch.rpm
