

%define contribName ninja 
%define contribVersion 1.8.2.g81279.kitware.dyndep-1.jobserver-1
%define contribPlatform Linux-x86_64
%define contribPlatformFixed Linux_x86_64
%define contribDir /opt/LHCbSoft/contrib
%define tarfilename ninja-1.8.2.g81279.kitware.dyndep-1.jobserver-1_x86_64-linux-gnu.tar.gz
%define urlPrefix https://github.com/Kitware/ninja/releases/download/v1.8.2.g81279.kitware.dyndep-1.jobserver-1/
%{!?tmproot: %define tmproot /tmp}
%define _topdir %{tmproot}/rpmbuild
%define tmpdir %{tmproot}/tmpbuild
%define _tmppath %{tmproot}/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot


Name: %{contribName}_%{contribVersion}
Version: 1.0.0
Release: 1
Vendor: Kitware
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: BSD
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: x86_64
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}


%description
%{contribName} %{contribVersion}


%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{contribDir}
if [ $? -ne 0 ]; then
  exit $?
fi

export tfname=%{tarfilename}
export mydirname=${tfname/.tar.gz/}

if [[ ! -f $tfname ]]; then
    wget %{urlPrefix}/%{tarfilename}
fi

if [[ ! -d $mydirname ]]; then
    tar xf %{tarfilename}
fi
mkdir -p  ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
cp -ar $mydirname/* ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}

%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
