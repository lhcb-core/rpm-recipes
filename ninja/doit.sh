#!/bin/sh -ex

SPEC=${1:-ninja-1.8.2.g81279.kitware.dyndep-1.jobserver-1.spec}

export TMPDIR=${TMPDIR:-/tmp}
mkdir -p $TMPDIR/rpmbuild/{BUILD,SOURCES,RPMS}

rpmbuild --define="tmproot $TMPDIR" -bb ${SPEC}

# clean up
mv $TMPDIR/rpmbuild/RPMS/*/ninja_*.rpm .
rm -rf $TMPDIR/rpmbuild
