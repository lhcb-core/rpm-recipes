#!/bin/bash
set -e

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
SCRIPT_LOC=$(cd  $dir;pwd)


export CMTCONFIG=${1:-x86_64-centos7-gcc8-opt}
export BINARY_TAG=${CMTCONFIG}
export COMPILER=${2:-8.2.0/x86_64-centos7}


# For gcc7: 7.3.0/x86_64-centos7/

echo "Building GitCondDB for ${CMTCONFIG}"

source /cvmfs/sft.cern.ch/lcg/releases/gcc/${COMPILER}/setup.sh
ARCH=$(uname -m)
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.13.4/Linux-${ARCH}/bin:${PATH}


if [ ! -d lcgcmake ]; then
  git clone -b LCG_95_LHCb https://gitlab.cern.ch/lhcb-core/lcgcmake.git
fi


if [ ! -d lcgjenkins ]; then
  git clone https://gitlab.cern.ch/sft/lcgjenkins.git
fi

if [ ! -d lcgcmake-build ]; then
  mkdir lcgcmake-build
  ( cd  lcgcmake-build 
  cmake -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases -DLCG_VERSION=95 -DLCG_IGNORE='GitCondDB;fmt' -DCMAKE_CXX_STANDARD=17 -DCMAKE_INSTALL_PREFIX=../lcgcmake-install ../lcgcmake )
fi
cd lcgcmake-build
make -j 32 GitCondDB 

cd ../lcgcmake-install/
../lcgjenkins/extract_LCG_summary.py . ${BINARY_TAG} 95 RELEASE
 ../lcgjenkins/LCGRPM/package/createLCGRPMSpec.py -p ${BINARY_TAG} --release=96 --match="(.*GitCondDB.*|.*fmt.*)" -o all.spec ./LCG_externals_${BINARY_TAG}.txt
rpmbuild -bb all.spec


