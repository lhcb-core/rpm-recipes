#!/bin/bash 
set -e 
cd $TMPDIR
if [ -d  DD4hep.git.temp  ]; then
    cd DD4hep.git.temp
    git fetch --quiet --all
else
    git clone --quiet --mirror https://github.com/AIDASoft/DD4hep.git DD4hep.git.temp    
    cd DD4hep.git.temp
fi
# should try ls-remote as well
git describe --tags master

