#!/bin/bash
set -e

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
SCRIPT_LOC=$(cd  $dir;pwd)

function build {    
	echo "Building GitCondDB for ${1} / ${2}"
	#singularity exec /cvmfs/cernvm-prod.cern.ch/cvm4 ${SCRIPT_LOC}/build_gitconddb_rpm.sh ${CMTCONFIG}
	${SCRIPT_LOC}/build_gitconddb_rpm.sh ${1} ${2}
	rm -rf lcgcmake-build
	rm -rf lcgcmake-install
}

function build_singularity {    
	echo "Building GitCondDB for ${1} / ${2}"
	echo singularity exec --bind /cvmfs --home `pwd` $3 ./build_gitconddb_rpm.sh ${1} ${2}
	singularity exec --bind /cvmfs --home `pwd` $3 ./build_gitconddb_rpm.sh ${1} ${2}
	rm -rf lcgcmake-build 
	rm -rf lcgcmake-install
}

cd ${SCRIPT_LOC}
build_singularity  x86_64-slc6-gcc8-opt 8.2.0/x86_64-slc6 /cvmfs/cernvm-prod.cern.ch/cvm3
build_singularity  x86_64-slc6-gcc8-dbg 8.2.0/x86_64-slc6 /cvmfs/cernvm-prod.cern.ch/cvm3
build_singularity x86_64-centos7-gcc8-opt 8.2.0/x86_64-centos7 /cvmfs/cernvm-prod.cern.ch/cvm4
build_singularity  x86_64-centos7-gcc8-dbg 8.2.0/x86_64-centos7 /cvmfs/cernvm-prod.cern.ch/cvm4
build_singularity  x86_64-centos7-gcc7-opt 7.3.0/x86_64-centos7 /cvmfs/cernvm-prod.cern.ch/cvm4
build_singularity  x86_64-centos7-gcc7-dbg 7.3.0/x86_64-centos7 /cvmfs/cernvm-prod.cern.ch/cvm4
