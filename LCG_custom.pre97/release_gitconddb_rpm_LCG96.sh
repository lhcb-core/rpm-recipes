#!/bin/bash
set -e

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
SCRIPT_LOC=$(cd  $dir;pwd)

function release_files {

    TORELEASE=$@

    echo "Going to release:"
    for f in ${TORELEASE}; do 
	echo "- ${f}"
    done

    TARGET=/eos/project/l/lhcbwebsites/www/lhcb-rpm/incubator
    for f in ${TORELEASE}; do
	echo "copying ${f} to ${TARGET}"
	cp  ${f} ${TARGET}
    done
}

release_files $(ls /tmp/rpmbuild/RPMS/noarch/*GitCondDB*)

echo "Updating RPM metadata"
createrepo --update ${TARGET}
