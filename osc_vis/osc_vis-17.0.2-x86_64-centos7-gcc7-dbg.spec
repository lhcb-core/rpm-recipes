
%define contribName osc_vis
%define contribVersion 17.0.2
%define contribPlatform x86_64-centos7-gcc7-dbg
%define contribPlatformFixed x86_64_centos7_gcc7_dbg
%define contribDir /opt/LHCbSoft/contrib
#%define CONTRIBROOT /afs/cern.ch/sw/contrib
%define CONTRIBROOT  /afs/cern.ch/work/g/gybarran/contrib 

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: %{contribName}_%{contribVersion}_%{contribPlatformFixed}
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}_%{contribPlatformFixed}


%description
%{contribName} %{contribVersion}


%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{contribDir}
if [ $? -ne 0 ]; then
  exit $?
fi

cd ${RPM_BUILD_ROOT}%{contribDir}
mkdir -p  ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
rsync -arz %{CONTRIBROOT}/%{contribName}/%{contribVersion}/%{contribPlatform}/ ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}

%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
