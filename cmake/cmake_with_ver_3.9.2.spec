
# Getting file from:
#https://cmake.org/files/v3.9/cmake-3.9.0-Linux-x86_64.tar.gz

%define contribName CMake
%define contribVersion 3.9.2
%define contribPlatform Linux-x86_64
%define contribPlatformFixed Linux_x86_64
%define contribDir /opt/LHCbSoft/contrib
%define tarfilename cmake-3.9.2-Linux-x86_64.tar.gz
%define urlPrefix https://cmake.org/files/v3.9
%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot


Name: %{contribName}_%{contribVersion}
Version: 1.0.0
Release: 1
Vendor: Kitware
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: BSD
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: x86_64
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}


%description
%{contribName} %{contribVersion}


%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{contribDir}
if [ $? -ne 0 ]; then
  exit $?
fi

export tfname=%{tarfilename}
export mydirname=${tfname/.tar.gz/}

if [[ ! -f $tfname ]]; then
    wget %{urlPrefix}/%{tarfilename}
fi

if [[ ! -d $mydirname ]]; then
    tar xf %{tarfilename}
fi
mkdir -p  ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
cp -ar $mydirname/* ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}

%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
