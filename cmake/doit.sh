#!/bin/sh -ex

SPEC=${1:-cmake-3.19.3.spec}

export TMPDIR=${TMPDIR:-/tmp}
mkdir -p $TMPDIR/rpmbuild/{BUILD,SOURCES,RPMS}

rpmbuild --define="tmproot $TMPDIR" -bb ${SPEC}

# clean up
mv $TMPDIR/rpmbuild/RPMS/*/CMake_*.rpm .
rm -rf $TMPDIR/rpmbuild
